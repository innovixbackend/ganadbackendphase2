<?php
/**
 * Upload class.
 * 
 * @package   Media_Library_Organizer_Zip
 * @author    WP Media Library
 * @version   1.0.0
 */
class Media_Library_Organizer_Upload {

    /**
     * Holds the base class object.
     *
     * @since   1.0.5
     *
     * @var     object
     */
    public $base;

    /**
     * Constructor
     * 
     * @since   1.0.5
     *
     * @param   object $base    Base Plugin Class
     */
    public function __construct( $base ) {

        // Store base class
        $this->base = $base;

        // Prepend the UI
        add_action( 'pre-upload-ui', array( $this, 'output_upload_ui' ) );

        // Define the pluploader's options
        add_filter( 'plupload_init', array( $this, 'plupload_options' ) );
        
        // Run actions when an attachment is uploaded
        add_action( 'add_attachment', array( $this, 'add_attachment' ) );

    }

    /**
     * Allows Addons to output below the HTML and JS uploaders at Media > Add New
     *
     * @since   1.0.5
     */
    public function output_upload_ui() {

        /**
         * Allows Addons to output below the HTML and JS uploaders at Media > Add New
         *
         * @since   1.0.5
         */
        do_action( 'media_library_organizer_upload_output_upload_ui' );

    }

    /**
     * Define the pluploader's options
     *
     * @since   1.0.5
     *
     * @param   array   $options    Plupload Options
     * @return  array               Plupload Options
     */
    public function plupload_options( $options ) {

        /**
         * Define the pluploader's options
         *
         * @since   1.0.5
         *
         * @param   array   $options    Plupload Options
         */
        $options = apply_filters( 'media_library_organizer_upload_plupload_options', $options );

        // Return
        return $options;

    }

    /**
     * Allows Addons to run actions on an attachment that has just been
     * uploaded to the WordPress Media Library
     *
     * @since   1.0.5
     */
    public function add_attachment( $attachment_id ) {

        /**
         * Allows Addons to run actions on an attachment that has just been
         * uploaded to the WordPress Media Library
         *
         * @since   1.0.5
         *
         * @param   int     $attachment_id  Attachment ID
         */
        do_action( 'media_library_organizer_upload_add_attachment', $attachment_id );

    }

}