=== Media Library Organizer ===
Contributors: wpmedialibrary
Donate link: https://wpmedialibrary.com
Tags: media categories, media organizer, media search, media library
Requires at least: 5.0
Tested up to: 5.5
Requires PHP: 5.6
Stable tag: trunk
License: GPLv2 or later

Categorize, Organize and Search your Media Library Images and Files, quicker and easier, with unlimited Categories.

== Description ==

= Media Library Organization Plugin =

Media Library Organizer is a simple, effective WordPress Plugin that allows you to categorize, organize and search images, video, 
other media and attachments in your WordPress Media Library.

There's no limit on the number of Categories or Subcategories.

[youtube https://www.youtube.com/watch?v=HkbiiBtIa_k]

= Features =

* Categorize images, video, other media and attachments
* Works with all Media Library views (Featured Image, Image Picker, Media Library List, Media Library Grid, Editing a Media Library item)
* Works with most <a href="https://wpmedialibrary.com/documentation/media-library-organizer/page-builders/" rel="friend">Page Builders</a>
* Tree View for easy filtering and Bulk Categorization with no limit on the number of Categories or Subcategories
* Search Media by Category
* Sort Media by Category, Date or Name
* Intuitive Category picker, seamlessly integrated into WordPress' native UI
* Import and Export Categories and Categorization Data from JSON or WordPress standards export file

= Page Builders =

Media Library Organizer's category filters and sorting options are supported in the following Page Builders:
* Ark (Theme)
* Avada 5.0+ (Theme)
* Avia Layout Builder (Plugin)
* Beaver Builder (Plugin)
* BeTheme 21.1.x+ (Theme)
* Bold Page Builder (Plugin)
* Divi 3.0+ (Theme) and The Divi Builder (Plugin)
* Elementor and Elementor Pro(Plugin)
* Enfold (Theme)
* Flatsome (Theme)
* Fusion Builder (Plugin)
* Fresh Builder (Plugin)
* Kallyas Theme (using Zion Page Builder)
* KuteThemes (Themes using Ovic Addons Toolkit Plugin)
* Live Composer (Plugin)
* Muffin Page Builder (Plugin)
* Oxygen Page Builder (Plugin)
* SiteOrigin Page Builder (Plugin)
* The7 (Theme)
* TheBuilt (Theme)
* Thrive Architect (Plugin)
* Visual Composer (Plugin)
* WPBakery Page Builder (Plugin)

This is not an exhaustive list; your Page Builder may work!  Media Library Organizer is coded to WordPress standards, ensuring best 
possible compatibility with other Themes and Plugins not listed here.  Feel free to try your Page Builder of choice, 
and contact us if you run into any issues.

= Migrations =

Media Library Organizer has in built importers, allowing you to migrate from other WordPress Media Library Plugins:

* <a href="https://wpmedialibrary.com/documentation/import-export/import-from-enhanced-media-library/" rel="friend" title="Import from Enhanced Media Library">Enhanced Media Library</a>
* <a href="https://wpmedialibrary.com/documentation/import-export/import-from-filebird/" rel="friend" title="Import from FileBird">FileBird</a>
* <a href="https://wpmedialibrary.com/documentation/import-export/import-from-folders-premio/" rel="friend" title="Import from Folders (Premio)">Folders (Premio)</a>
* <a href="https://wpmedialibrary.com/documentation/import-export/import-from-happyfiles/" rel="friend" title="Import from HappyFiles">HappyFiles</a>
* <a href="https://wpmedialibrary.com/documentation/import-export/import-from-wicked-folders/" rel="friend" title="Import from Wicked Folders">Wicked Folders</a>

> #### Media Library Organizer Pro
> <a href="https://wpmedialibrary.com/pricing" rel="friend" title="Media Library Organizer Pro">Media Library Organizer Pro</a> provides additional functionality:<br />
>
> - **<a href="https://wpmedialibrary.com/features/auto-categorize-images/" rel="friend" title="Auto Categorize Media in WordPress">Auto Categorization</a>:** Automatically categorize images uploaded through WordPress using image recognition<br />
> - **<a href="https://wpmedialibrary.com/features/bulk-quick-edit-media/" rel="friend" title="Auto Categorize Media in WordPress">Bulk and Quick Edit Media</a>:** Bulk and Quick Edit Titles, Descriptions, Categories and more<br />
> - **<a href="https://wpmedialibrary.com/features/default-attributes/" rel="friend" title="Auto Categorize Media in WordPress">Default Attributes</a>:** Define Default Titles, Alt Tags, Captions, Descriptions and Categories for newly uploaded files where no data is specified<br />
> - **<a href="https://wpmedialibrary.com/features/duplicate-attachments/" rel="friend" title="Duplicate WordPress Attachments">Duplicate Attachments</a>:** Duplicate one or more attachments in the Media Library<br />
> - **<a href="https://wpmedialibrary.com/features/dynamic-galleries/" rel="friend" title="Auto Categorize Media in WordPress">Dynamic Galleries</a>:** Extend WordPress' [gallery] shortcode, building paginated, dynamic galleries by Media Category, Author, Search Terms and more.<br />
> - **<a href="https://wpmedialibrary.com/features/exif-iptc/" rel="friend" title="Auto Categorize Media in WordPress">EXIF and IPTC</a>:** Read, write and display EXIF and IPTC image data<br />
> - **<a href="https://wpmedialibrary.com/features/output/" rel="friend" title="Customize Media Library Output">Output</a>:** Change the size of thumbnails in the Media Library<br />
> - **<a href="https://wpmedialibrary.com/features/zip-unzip/" rel="friend" title="Auto Categorize Media in WordPress">ZIP and Unzip</a>:** Automatically unzip files when uploaded to the Media Library, and zip multiple Media Library files<br />
>
> - **<a href="https://wpmedialibrary.com/support" rel="friend" title="WordPress Media Library Support">Support</a>**: Access to one on one email support<br />
> - **<a href="https://wpmedialibrary.com/documentation" rel="friend" title="WordPress Media Library Support">Documentation</a>**: Detailed documentation on how to install and configure the plugin<br />
> - **Updates**<br />Receive one click update notifications, right within your WordPress Adminstration panel<br />
> - **Seamless Upgrade**<br />Retain all current settings when upgrading to Pro<br />
>
> [Upgrade to Media Library Organizer Pro](https://wpmedialibrary.com/pricing/)

= Support =

We will do our best to provide support through the WordPress forums. However, please understand that this is a free plugin, 
so support will be limited. Please read this article on <a href="http://www.wpbeginner.com/beginners-guide/how-to-properly-ask-for-wordpress-support-and-get-it/">how to properly ask for WordPress support and get it</a>.

If you require one to one email support, please consider <a href="http://wpmedialibrary.com/pricing" rel="friend">upgrading to the Pro version</a>.

= Fully Documented =

We understand that no WordPress Plugin is good if you don't know how to use it.  That's why we provide extensive, full documentation
covering all aspects of Media Library Organizer:

<a href="https://wpmedialibrary.com/documentation" title="Media Library Organizer Documentation">https://wpmedialibrary.com/documentation</a>

You'll also find contextual Documentation links from within Media Library Organizer's interface.

== Installation ==

1. Install Media Library Organizer via the Plugins > Add New section of your WordPress Installation, or by uploading the downloaded
ZIP file via Plugins > Add New > Upload Plugin.
2. Active the Media Library Organizer plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==



== Screenshots ==

1. Categorization Filters and Sort Filters on Media Screen
2. Edit Categories when editing Media
3. Manage Categories
4. Plugin-wide Settings

== Changelog ==

= 1.1.6 (2020-07-30) =
* Added: Attachment: Get/set filename
* Fix: Settings: Use AJAX to save statuses to avoid settings not saving or changing when PHP's max_input_vars is exceeded

= 1.1.5 (2020-07-17) =
* Added: Tree View: Enable by default on new installations
* Added: Tree View: Inline notification displayed when Attachments are categorized successfully
* Fix: Tree View: Drag and drop would fail after the first categorization
* Fix: Tree View: Position "Categorize X Items" relative to cursor when dragging Attachments

= 1.1.4 (2020-07-16) =
* Added: Elementor: Category Filter and Order / Order By options in Insert Media Modal when editing a Page with Elementor
* Added: Thrive Architect: Category Filter and Order / Order By options in Insert Media Modal when editing a Page with Thrive Architect
* Added: Tree View, including Bulk Adding Media to Categories.  See Docs: https://wpmedialibrary.com/documentation/tree-view
* Fix: List View: Display Selected Category in Media Categories Dropdown when a Category was clicked on in the table's Media Categories column 
* Fix: Grid View: Display Selected Category in Media Categories Dropdown when a Category was clicked on in the Tree View
* Fix: Tree View: List View: Update Attachment Media Categories Name and Link when a Category is edited in the Tree View
* Fix: Tree View: List View: Remove Attachment Media Categories Name and Link when a Category is deleted in the Tree View
* Fix: Tree View: Grid View: Set selected state of a Category in the sidebar
* Fix: Tree View: Grid View: Update Attachment Media Categories Name and Link when a Category is edited in the Tree View
* Fix: Tree View: Grid View: Remove Attachment Media Categories Name and Link when a Category is deleted in the Tree View
* Fix: Frontend Page Builders: Prevent Theme styles from overriding Attachment Modal

= 1.1.3 (2020-07-09) =
* Added: Import & Export: Import from HappyFiles.  See Docs: https://wpmedialibrary.com/documentation/import-export/import-from-happyfiles/
* Fix: Grid View / Modal: Edit Attachment: Don't display Media Categories if the User's Role doesn't permit editing Attachments

= 1.1.2 (2020-07-02) =
* Added: Import & Export: Import from FileBird.  See Docs: https://wpmedialibrary.com/documentation/import-export/import-from-filebird/
* Added: Import & Export: Import from Wicked Folders.  See Docs: https://wpmedialibrary.com/documentation/import-export/import-from-wicked-folders/
* Added: Import & Export: Import from Folders (Premio).  See Docs: https://wpmedialibrary.com/documentation/import-export/import-from-folders-premio/

= 1.1.1 (2020-06-25) =
* Added: Filters: Option to enable or disable File Types filter
* Added: Filters: File Type: Documents, Spreadsheets, Presentations and Text File Types
* Fix: JS and CSS: Differentiate between Upload/Media and Edit Attachment screens for performance
* Fix: Minified Javascript and CSS for performance

= 1.1.0 (2020-06-04) =
* Fix: Settings: General: Autocomplete Search option wouldn't always display for Media Categories Dropdown when enabled

= 1.0.9 =
* Added: Settings: General: Autocomplete Search option for Media Categories Dropdown
* Fix: Settings: Change General tab CSS class to avoid conflict with WordPress 5.3.2
* Fix: Settings: Accessibility: Replaced Titles with <label> elements that focus the given input element on click

= 1.0.8 =
* Added: Integration with Divi Frontend Builder

= 1.0.7 =
* Added: Integration with Beaver Builder
* Added: Integration with Kallyas Theme's Zion Builder

= 1.0.6 =
* Fix: PHP error: Trying to get property term_id of non-object

= 1.0.5 =
* Added: Settings: Notices can be dismissed / closed
* Fix: Code changes to improve performance
* Fix: CSS / JS: Enqueue on all screens in the Media section of the WordPress Admin UI

= 1.0.4 =
* Fix: Multisite: Network Activation: Ensure installation routines are automatically run on all existing sites
* Fix: Multisite: Network Activation: Ensure installation routines are automatically run on new sites created after Network Activation of Plugin
* Fix: Multisite: Site Activation: Ensure installation routines are automatically run
* Fix: Multisite: Network Deactivation: Ensure uninstallation routines are automatically run on all existing sites
* Fix: Deactivation: PHP warnings
* Fix: Changing a media item's Categories in Grid View would result in console errors and not saving changes

= 1.0.3 =
* Added: Settings: UI Enhancements to allow for a larger number of setting tabs
* Added: Import & Export: UI Enhancements to allow for a larger number of setting tabs
* Fix: Gutenberg: Select or Upload Media: Sparodic issue where cached media queries would produce no results

= 1.0.2 =
* Fix: PHP Warning: call_user_func_array() expects parameter 1 to be a valid callback, class 'Media_Library_Organizer_Install' does not have a method 'install_shutdown' 
* Fix: PHP Notice: Undefined index: post_type in /wp-content/plugins/media-library-organizer/includes/admin/media.php on line 313
* Fix: PHP Notice: Trying to get property of non-object in /wp-content/plugins/media-library-organizer/includes/admin/media.php on line 414

= 1.0.1 =
* Added: Review Helper
* Added: Media View Mode (list or grid) is included in Admin Screen calls for Addons
* Fix: Removed unused upgrade() call
* Fix: Corrected Author information
* Fix: Settings > User Options were not always honored
* Fix: Settings > User Options > Sort Order description
* Fix: Media Library > Grid View > Order By value set to descending by default

= 1.0.0 =
* First release.

== Upgrade Notice ==
