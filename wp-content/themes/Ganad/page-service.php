<?php
    get_template_part('otherheader');
    
?>
    <section class="service-section service-wrapper-margin">
        <div class="container">
          <div class="row">
            <?php
              get_template_part('Partial/_whatwedo'); 
              get_template_part('Partial/_servicelist'); 
            ?>
          </div>
        </div>
      </section>
<?php
    get_footer(); 
?>