<?php 
    include_once "sendemail.php";
    include_once "otherheader.php";
    $category="";
    $mediaCode="";
    if (isset($_GET['category'])){
      $category = $_GET['category'];
    }
    if (isset($_GET['mediaCode'])){
      $mediaCode = $_GET['mediaCode'];
    }

    if(have_posts()):
      while(have_posts()):
        the_post();
        $page_title = get_field("page_title");
        $address = get_field("address");
        $google_map_link = get_field("google_map_link");
        $phone_number_1 = get_field("phone_number_1");
        $phone_number_2 = get_field("phone_number_2");
        $email = get_field("email");
        $facebook = get_field("facebook");
        $instagram = get_field("instagram");
        $twitter = get_field("twitter");
        $youtube = get_field("youtube");
        $linkedin = get_field("linkedin");
        $host = host();
?>

<section class="contactus">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h3 class="sub-header upper pageheader"><?= $page_title ?></h3>
      </div>
    </div>
    <div class="row contact-form">
      <div class="col-md-6">
        <h5 class="title page-title"><?php echo __("enquiry", "ganad")  ?></h5>
        <div class="contact-from">
        <?php
          if($_POST['submit']) {
            $recaptcha =false;
            $success = false;
            if(isset($_POST['g-recaptcha-response'])){
                $captcha=$_POST['g-recaptcha-response'];
              }
            if(!$captcha){
               $recaptcha=true;
            }

            $secretKey = "6LcNtroZAAAAAJDhqzDKfuZEx88XyNxvkbtx_xoO";
            // post request to server
            $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secretKey) .  '&response=' . urlencode($captcha);
            $response = file_get_contents($url);
            $responseKeys = json_decode($response,true);
            // should return JSON with success as true
            if($responseKeys["success"])
            {
              $name = Validation($_POST['username']);
              $jobposition = Validation($_POST['jobposition']);
              $email = Validation($_POST['email']);
              $phone = Validation($_POST['phone']);
              $company = Validation($_POST['company']);
              $companysize = Validation($_POST['companysize']);
              $country = Validation($_POST['country']);
              $inquiry = preg_replace('#^https?\:\/\/([\w*\.]*)#', '',  $_POST['inquiry']);
              $success = contactemail($category,$mediaCode,$name, $jobposition, $email, $phone,$company,$companysize,$country,$inquiry);
            }
          
            
          }
          if($recaptcha){
            ?>
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Error. </strong> Please fill google reCaptcha.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <?php
          }
            if($success)
            {
          ?>
          <div class="alert alert-success alert-dismissible fade show" role="alert">
          <strong> Your email have been sent.Thanks for contacting us!</strong>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
          </div>
        <?php }?>

          <form action="" method="POST">
            <div class="form-group">
              <label for="name"><?php echo __("name", "ganad")  ?></label><span class="star">*</span>
              <input class="form-control"  type="text" name="username" required >
            </div>
            <div class="form-group">
              <label for="email"><?php echo __("job_position", "ganad")  ?></label>
              <input class="form-control" type="text" name="jobposition" >
            </div>
            <div class="form-group">
              <label for="email"><?php echo __("email", "ganad")  ?></label><span class="star">*</span>
              <input class="form-control" type="email" name="email" required>
            </div>
            <div class="form-group">
              <label for="phone"><?php echo __("phone", "ganad")  ?></label><span class="star">*</span>
              <input class="form-control" type="text" name="phone" required>
            </div>
            <div class="form-group">
              <label for="phone"><?php echo __("company", "ganad")  ?></label><span class="star">*</span>
              <input class="form-control" type="text" name="company" required>
            </div>
            <div class="form-group">
              <label for="sel1"><?php echo __("company_size", "ganad")  ?></label><span class="star">*</span>
              <select class="form-control" name="companysize">
                <option value="Less than 10 employees">Less than 10 employees</option>
                <option value="10-20 employees">10-20 employees</option>
                <option value="20-100 employees">20-100 employees</option>
                <option value="100+ employees">100+ employees</option>
              </select>
            </div>
            <div class="form-group">
              <label for="sel1"><?php echo __("country", "ganad")  ?></label><span class="star">*</span>
              <select class="form-control" name="country">
                <option value="Afghanistan">Afghanistan</option>
                <option value="Åland Islands">Åland Islands</option>
                <option value="Albania">Albania</option>
                <option value="Algeria">Algeria</option>
                <option value="American Samoa">American Samoa</option>
                <option disabled="1" value="Americas">Americas</option>
                <option value="Andorra">Andorra</option>
                <option value="Angola">Angola</option>
                <option value="Anguilla">Anguilla</option>
                <option value="Antarctica">Antarctica</option>
                <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                <option value="Argentina">Argentina</option>
                <option value="Armenia">Armenia</option>
                <option value="Aruba">Aruba</option>
                <option disabled="1" value="Asia &amp; Oceania">Asia &amp; Oceania</option>
                <option value="Australia">Australia</option>
                <option value="Austria">Austria</option>
                <option value="Azerbaijan">Azerbaijan</option>
                <option value="Bahamas">Bahamas</option>
                <option value="Bahrain">Bahrain</option>
                <option value="Bangladesh">Bangladesh</option>
                <option value="Barbados">Barbados</option>
                <option value="Belarus">Belarus</option>
                <option value="Belgium">Belgium</option>
                <option value="Belize">Belize</option>
                <option value="Benin">Benin</option>
                <option value="Bermuda">Bermuda</option>
                <option value="Bhutan">Bhutan</option>
                <option value="Bolivia">Bolivia</option>
                <option value="Bonaire, Sint Eustatius and Saba">Bonaire, Sint Eustatius and Saba</option>
                <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                <option value="Botswana">Botswana</option>
                <option value="Bouvet Island">Bouvet Island</option>
                <option value="Brazil">Brazil</option>
                <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                <option value="Brunei Darussalam">Brunei Darussalam</option>
                <option value="Bulgaria">Bulgaria</option>
                <option value="Burkina Faso">Burkina Faso</option>
                <option value="Burundi">Burundi</option>
                <option value="Cambodia">Cambodia</option>
                <option value="Cameroon">Cameroon</option>
                <option value="Canada">Canada</option>
                <option value="Cape Verde">Cape Verde</option>
                <option value="Cayman Islands">Cayman Islands</option>
                <option value="Central African Republic">Central African Republic</option>
                <option value="Chad">Chad</option>
                <option value="Chile">Chile</option>
                <option value="China">China</option>
                <option value="China">China</option>
                <option value="Christmas Island">Christmas Island</option>
                <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                <option value="Colombia">Colombia</option>
                <option value="Comoros">Comoros</option>
                <option value="Congo">Congo</option>
                <option value="Cook Islands">Cook Islands</option>
                <option value="Costa Rica">Costa Rica</option>
                <option value="Côte d'Ivoire">Côte d'Ivoire</option>
                <option value="Croatia">Croatia</option>
                <option value="Cuba">Cuba</option>
                <option value="Curaçao">Curaçao</option>
                <option value="Cyprus">Cyprus</option>
                <option value="Czech Republic">Czech Republic</option>
                <option value="Democratic Republic of the Congo">Democratic Republic of the Congo</option>
                <option value="Denmark">Denmark</option>
                <option value="Djibouti">Djibouti</option>
                <option value="Dominica">Dominica</option>
                <option value="Dominican Republic">Dominican Republic</option>
                <option value="Ecuador">Ecuador</option>
                <option value="Egypt">Egypt</option>
                <option value="El Salvador">El Salvador</option>
                <option value="Equatorial Guinea">Equatorial Guinea</option>
                <option value="Eritrea">Eritrea</option>
                <option value="Estonia">Estonia</option>
                <option value="Ethiopia">Ethiopia</option>
                <option value="Falkland Islands">Falkland Islands</option>
                <option value="Faroe Islands">Faroe Islands</option>
                <option value="Fiji">Fiji</option>
                <option value="Finland">Finland</option>
                <option value="France">France</option>
                <option value="French Guiana">French Guiana</option>
                <option value="French Polynesia">French Polynesia</option>
                <option value="French Southern Territories">French Southern Territories</option>
                <option value="Gabon">Gabon</option>
                <option value="Gambia">Gambia</option>
                <option value="Georgia">Georgia</option>
                <option value="Germany">Germany</option>
                <option value="Ghana">Ghana</option>
                <option value="Gibraltar">Gibraltar</option>
                <option value="Greece">Greece</option>
                <option value="Greenland">Greenland</option>
                <option value="Grenada">Grenada</option>
                <option value="Guadeloupe">Guadeloupe</option>
                <option value="Guam">Guam</option>
                <option value="Guatemala">Guatemala</option>
                <option value="Guernsey">Guernsey</option>
                <option value="Guinea">Guinea</option>
                <option value="Guinea-Bissau">Guinea-Bissau</option>
                <option value="Guyana">Guyana</option>
                <option value="Haiti">Haiti</option>
                <option value="Heard Island and McDonald Islands">Heard Island and McDonald Islands</option>
                <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                <option value="Honduras">Honduras</option>
                <option value="Hong Kong">Hong Kong</option>
                <option value="Hungary">Hungary</option>
                <option value="Iceland">Iceland</option>
                <option value="India">India</option>
                <option value="Indonesia">Indonesia</option>
                <option value="Iran">Iran</option>
                <option value="Iraq">Iraq</option>
                <option value="Ireland">Ireland</option>
                <option value="Isle of Man">Isle of Man</option>
                <option value="Israel">Israel</option>
                <option value="Italy">Italy</option>
                <option value="Jamaica">Jamaica</option>
                <option value="Japan">Japan</option>
                <option value="Jersey">Jersey</option>
                <option value="Jordan">Jordan</option>
                <option value="Kazakhstan">Kazakhstan</option>
                <option value="Kenya">Kenya</option>
                <option value="Kiribati">Kiribati</option>
                <option value="Kosovo">Kosovo</option>
                <option value="Kuwait">Kuwait</option>
                <option value="Kyrgyzstan">Kyrgyzstan</option>
                <option value="Laos">Laos</option>
                <option value="Latvia">Latvia</option>
                <option value="Lebanon">Lebanon</option>
                <option value="Lesotho">Lesotho</option>
                <option value="Liberia">Liberia</option>
                <option value="Libya">Libya</option>
                <option value="Liechtenstein">Liechtenstein</option>
                <option value="Lithuania">Lithuania</option>
                <option value="Luxembourg">Luxembourg</option>
                <option value="Macau">Macau</option>
                <option value="Macedonia, the former Yugoslav Republic of">Macedonia, the former Yugoslav Republic of</option>
                <option value="Madagascar">Madagascar</option>
                <option value="Malawi">Malawi</option>
                <option value="Malaysia">Malaysia</option>
                <option value="Maldives">Maldives</option>
                <option value="Mali">Mali</option>
                <option value="Malta">Malta</option>
                <option value="Marshall Islands">Marshall Islands</option>
                <option value="Martinique">Martinique</option>
                <option value="Mauritania">Mauritania</option>
                <option value="Mauritius">Mauritius</option>
                <option value="Mayotte">Mayotte</option>
                <option value="Mexico">Mexico</option>
                <option value="Micronesia">Micronesia</option>
                <option value="Moldova">Moldova</option>
                <option value="Monaco">Monaco</option>
                <option value="Mongolia">Mongolia</option>
                <option value="Montenegro">Montenegro</option>
                <option value="Morocco">Morocco</option>
                <option value="Mozambique">Mozambique</option>
                <option value="Myanmar" selected>Myanmar</option>
                <option value="Namibia">Namibia</option>
                <option value="Nepal">Nepal</option>
                <option value="Netherlands">Netherlands</option>
                <option value="New Caledonia">New Caledonia</option>
                <option value="New Zealand">New Zealand</option>
                <option value="Nicaragua">Nicaragua</option>
                <option value="Niger">Niger</option>
                <option value="Nigeria">Nigeria</option>
                <option value="Niue">Niue</option>
                <option value="Norfolk Island">Norfolk Island</option>
                <option value="North Korea">North Korea</option>
                <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                <option value="Norway">Norway</option>
                <option value="Oman">Oman</option>
                <option disabled="1" value="Other countries">Other countries</option>
                <option value="Pakistan">Pakistan</option>
                <option value="Palau">Palau</option>
                <option value="Panama">Panama</option>
                <option value="Papua New Guinea">Papua New Guinea</option>
                <option value="Paraguay">Paraguay</option>
                <option value="Peru">Peru</option>
                <option value="Philippines">Philippines</option>
                <option value="Pitcairn Islands">Pitcairn Islands</option>
                <option value="Poland">Poland</option>
                <option value="Portugal">Portugal</option>
                <option value="Puerto Rico">Puerto Rico</option>
                <option value="Qatar">Qatar</option>
                <option value="Réunion">Réunion</option>
                <option value="Romania">Romania</option>
                <option value="Russian Federation">Russian Federation</option>
                <option value="Rwanda">Rwanda</option>
                <option value="Saint Barthélémy">Saint Barthélémy</option>
                <option value="Saint Helena, Ascension and Tristan da Cunha">Saint Helena, Ascension and Tristan da Cunha</option>
                <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                <option value="Saint Lucia">Saint Lucia</option>
                <option value="Saint Martin (French part)">Saint Martin (French part)</option>
                <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                <option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
                <option value="Samoa">Samoa</option>
                <option value="San Marino">San Marino</option>
                <option value="São Tomé and Príncipe">São Tomé and Príncipe</option>
                <option value="Saudi Arabia">Saudi Arabia</option>
                <option value="Senegal">Senegal</option>
                <option value="Serbia">Serbia</option>
                <option value="Seychelles">Seychelles</option>
                <option value="Sierra Leone">Sierra Leone</option>
                <option value="Singao">Singao</option>
                <option value="Singapore">Singapore</option>
                <option value="Sint Maarten (Dutch part)">Sint Maarten (Dutch part)</option>
                <option value="Slovakia">Slovakia</option>
                <option value="Slovenia">Slovenia</option>
                <option value="Solomon Islands">Solomon Islands</option>
                <option value="Somalia">Somalia</option>
                <option value="South Africa">South Africa</option>
                <option value="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option>
                <option value="South Korea">South Korea</option>
                <option value="South Sudan">South Sudan</option>
                <option value="Spain">Spain</option>
                <option value="Sri Lanka">Sri Lanka</option>
                <option value="State of Palestine">State of Palestine</option>
                <option value="Sudan">Sudan</option>
                <option value="Suriname">Suriname</option>
                <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                <option value="Swaziland">Swaziland</option>
                <option value="Sweden">Sweden</option>
                <option value="Switzerland">Switzerland</option>
                <option value="Syria">Syria</option>
                <option value="Taiwan">Taiwan</option>
                <option value="Tajikistan">Tajikistan</option>
                <option value="Tanzania">Tanzania</option>
                <option value="Thailand">Thailand</option>
                <option value="Timor-Leste">Timor-Leste</option>
                <option value="Togo">Togo</option>
                <option value="Tokelau">Tokelau</option>
                <option value="Tonga">Tonga</option>
                <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                <option value="Tunisia">Tunisia</option>
                <option value="Turkey">Turkey</option>
                <option value="Turkmenistan">Turkmenistan</option>
                <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                <option value="Tuvalu">Tuvalu</option>
                <option value="Uganda">Uganda</option>
                <option value="Ukraine">Ukraine</option>
                <option value="United Arab Emirates">United Arab Emirates</option>
                <option value="United Kingdom">United Kingdom</option>
                <option value="United States">United States</option>
                <option value="Uruguay">Uruguay</option>
                <option value="USA Minor Outlying Islands">USA Minor Outlying Islands</option>
                <option value="Uzbekistan">Uzbekistan</option>
                <option value="Vanuatu">Vanuatu</option>
                <option value="Venezuela">Venezuela</option>
                <option value="Vietnam">Vietnam</option>
                <option value="Virgin Islands (British)">Virgin Islands (British)</option>
                <option value="Virgin Islands (USA)">Virgin Islands (USA)</option>
                <option value="Wallis and Futuna">Wallis and Futuna</option>
                <option value="Western Sahara">Western Sahara</option>
                <option value="Yemen">Yemen</option>
                <option value="Zambia">Zambia</option>
                <option value="Zimbabwe">Zimbabwe</option>
              </select>
            </div>
            <div class="form-group">
              <label for="message"><?php echo __("your_inquiry", "ganad")  ?></label>
              <textarea class="form-control" name="inquiry" rows="5"></textarea>
            </div>
            <div class="form-group g-recaptcha" data-sitekey="6LcNtroZAAAAAGCKeKokk7ZJYkieDoCyoulqkU3k"></div>
            <div class="form-group">
              <button class="submit-btn" type="submit" name="submit" value="submit"><?php echo __("submit", "ganad")  ?></button>
            </div>
          </form>
        </div>
      </div>
      <div class="col-md-6 visit-us wrapper">
        <h5 class="title page-title"><?php echo __("visit_us", "ganad")  ?></h5>
        <p class="contact-address">
          <a href="<?=$google_map_link ?>" target="_blank"><?= nl2br($address) ?></a>
        <div class="contact-map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3818.7705564018484!2d96.16962951481823!3d16.83773488841012!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30c19340d21ef7dd%3A0x1681858a0e768aec!2sMyanmar%20Ganad%20Advertising!5e0!3m2!1sen!2smm!4v1595573917847!5m2!1sen!2smm" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>
        <div class="contact-detail">
          <h5 class="title"><?php echo __("contact_detail", "ganad")  ?></h5>
          <div class="detail-wraper">
            <div class="detail-wrap">
              <div class="img-wrap"><img class="img-fluid" src="<?php bloginfo('template_url'); ?>/assets/images/icons/phone-b.svg" alt=""></div>
              <div class="text-wrap"><a href="tel:<?= $phone_number_1 ?>"><?= $phone_number_1 ?></a> , <a href="tel:<?= $phone_number_2 ?>"><?= $phone_number_2 ?></a></div>
            </div>
            <div class="detail-wrap">
              <div class="img-wrap"><img class="img-fluid" src="<?php bloginfo('template_url'); ?>/assets/images/icons/letter-b.svg" alt=""></div>
              <div class="text-wrap"><a href="mailto:<?= $email ?>"><?= $email ?></a></div>
            </div>
            <?php
              if (!empty($facebook))
              {
            ?>
            <div class="detail-wrap">
              <div class="img-wrap"><img class="img-fluid" src="<?php bloginfo('template_url'); ?>/assets/images/icons/facebook-b.svg" alt=""></div>
              <div class="text-wrap"><a href="<?= $facebook ?>" target="_blank">Follow us on facebook</a></div>
            </div>
            <?php
            }
              if (!empty($instagram))
              {
            ?>
            <div class="detail-wrap">
              <div class="img-wrap"><img class="img-fluid" src="<?php bloginfo('template_url'); ?>/assets/images/icons/instagram-b.svg" alt=""></div>
              <div class="text-wrap"><a href="<?= $instagram ?>" target="_blank">Follow us on Instagram</a></div>
            </div>
            <?php
            }
              if (!empty($twitter))
              {
            ?>
             <div class="detail-wrap">
              <div class="img-wrap"><img class="img-fluid" src="<?php bloginfo('template_url'); ?>/assets/images/icons/twitter-b.svg" alt=""></div>
              <div class="text-wrap"><a href="<?= $twitter ?>" target="_blank">Follow us on Twitter</a></div>
            </div>
            <?php
            }
              if (!empty($youtube))
              {
            ?>
             <div class="detail-wrap">
              <div class="img-wrap"><img class="img-fluid" src="<?php bloginfo('template_url'); ?>/assets/images/icons/youtube_c.svg" alt=""></div>
              <div class="text-wrap"><a href="<?= $youtube ?>" target="_blank">Follow us on Youtube</a></div>
            </div>
            <?php
            }
              if (!empty($linkedin))
              {
            ?>
             <div class="detail-wrap">
              <div class="img-wrap"><img class="img-fluid" src="<?php bloginfo('template_url'); ?>/assets/images/icons/linkedin_c.svg" alt=""></div>
              <div class="text-wrap"><a href="<?= $linkedin ?>" target="_blank">Follow us on LinkedIn</a></div>
            </div>
            <?php
            }
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php
    endwhile;
  endif;
  get_footer();
?>

<!-- <script defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC5i0mt34lAKRc2J59PlC-M9T6TPyk817g&amp;callback=InitMap"></script> -->
<!-- <script defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyhLId3Kwr3MaJKoABzzuePVbcltp6Q-4&amp;callback=InitMap"></script>
<script>
  InitMap = () => {
    let map = new google.maps.Map(document.getElementById('contact-map'), {
      center: {lat:16.838012, lng: 96.171850},
      zoom: 17
    })
    let cursors = [
      {
        "Longitude":"96.171850",
        "Latitude":"16.838012",
        "MediaCode":"Myanmar Ganad Advertising",
        "Location":"37 Aung Chan Thar Road, Yankin Townshop, ရန်ကုန် 11081"
      }
    ]
  
    cursors.map(cursor => {
      let marker = new google.maps.Marker({
        position: {
          lat: Number(cursor.Latitude),
          lng: Number(cursor.Longitude)
        }, 
        map,
        title: cursor.MediaCode
      })
      var infowindow = new google.maps.InfoWindow({
        content: `
          <h5>${cursor.MediaCode}</h5>
          <p>
            ${cursor.Location}
          </p>
        `
      });
      marker.addListener('click', function() {
        infowindow.open(map, marker);
      })
    })
  }
</script> -->
