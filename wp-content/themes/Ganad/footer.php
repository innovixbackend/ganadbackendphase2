<?php
  $query = new WP_Query('pagename=site-setting');
  if($query->have_posts()){
      while ($query->have_posts()) {
      $query->the_post();

      $logo = GetImage("logo","large");
      } 
 }

 $medianetwork = new WP_Query('pagename=media-network');
  if($medianetwork->have_posts()){
      while ($medianetwork->have_posts()) {
      $medianetwork->the_post();
      $page_title = get_field("page_title");
      } 
 }

 $contact = new WP_Query('pagename=contact');
 if($contact->have_posts()){
     while ($contact->have_posts()) {
     $contact->the_post();

        $address = get_field("address");
        $google_map_link = get_field("google_map_link");
        $phone_number_1 = get_field("phone_number_1");
        $phone_number_2 = get_field("phone_number_2");
        $email = get_field("email");
        $facebook = get_field("facebook");
        $instagram = get_field("instagram");
        $twitter = get_field("twitter");
        $youtube = get_field("youtube");
        $linkedin = get_field("linkedin");
     } 
}

?>
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root" class="fa-popup"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v8.0'
          });
        };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

      <!-- Your Chat Plugin code -->
      <div class="fb-customerchat"
        attribution=setup_tool
        page_id="598566510541709"
  theme_color="#ffc300"
  logged_in_greeting="Hi! How can we help you?"
  logged_out_greeting="Hi! How can we help you?">
      </div>
      
<footer class="footer">
        <div class="container">
          <div class="row">
            <div class="col-12 col-xl-2 mb-3">
              <a class="footer-border" href="<?= home_url('./') ?>">
                <div class="footer-contact-wrap img-wrap">
                  <img class="footer-logo" src="<?= $logo ?>" alt="">
                </div>
              </a>
              <span class="upper"><?= $page_title ?></span>
            </div>
            <div class="col-12 col-xl-4">
              <div class="footer-border">
                <div class="footer-contact-wrap">
                  <div class="contact-img"><img src="<?php bloginfo('template_url'); ?>/assets/images/icons/phone.svg" alt=""></div>
                  <div class="footer-contact">
                    <a href="tel:<?= $phone_number_1 ?>"><?= $phone_number_1 ?></a>
                    <a href="tel:<?= $phone_number_2 ?>"><?= $phone_number_2 ?></a>
                  </div>
                </div>
                <div class="footer-contact-wrap">
                  <div class="contact-img"><img src="<?php bloginfo('template_url'); ?>/assets/images/icons/location.svg" alt=""></div>
                  <div class="footer-contact">
                    <p> 
                    <a href="<?=$google_map_link ?>" target="_blank"><?= nl2br($address) ?></a>
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-xl-4">
              <div class="footer-border third-col">
                <div class="footer-contact-wrap">
                  <div class="contact-img"><img src="<?php bloginfo('template_url'); ?>/assets/images/icons/letter.svg" alt=""></div>
                  <div class="footer-contact">
                    <a href="mailto:<?= $email ?>"><?= $email ?></a>
                  </div>
                </div>
                <?php
                  if (!empty($facebook))
                  {
                ?>
                <div class="footer-contact-wrap">
                  <div class="contact-img"><img src="<?php bloginfo('template_url'); ?>/assets/images/icons/facebook.svg" alt=""></div>
                  <div class="footer-contact"><a href="<?= $facebook ?>" target="_blank">Follow us on Facebook</a></div>
                </div>
                <?php
                }
                  if (!empty($instagram))
                  {
                ?>
                <div class="footer-contact-wrap">
                  <div class="contact-img"><img src="<?php bloginfo('template_url'); ?>/assets/images/icons/instagram-sketched.svg" alt=""></div>
                  <div class="footer-contact"><a href="<?= $instagram ?>" target="_blank">Follow us on Instagram</a></div>
                </div>
                <?php
                }
                  if (!empty($twitter))
                  {
                ?>
                <div class="footer-contact-wrap">
                  <div class="contact-img"><img src="<?php bloginfo('template_url'); ?>/assets/images/icons/twitter.svg" alt=""></div>
                  <div class="footer-contact"><a href="<?= $twitter ?>" target="_blank">Follow us on Twitter</a></div>
                </div>
                 <?php
                }
                  if (!empty($youtube))
                  {
                ?>
                <div class="footer-contact-wrap">
                  <div class="contact-img"><img src="<?php bloginfo('template_url'); ?>/assets/images/icons/youtube_f.svg" alt=""></div>
                  <div class="footer-contact"><a href="<?= $youtube ?>" target="_blank">Follow us on Youtube</a></div>
                </div>
                <?php
                }
                  if (!empty($linkedin))
                  {
                ?>
                <div class="footer-contact-wrap">
                  <div class="contact-img"><img src="<?php bloginfo('template_url'); ?>/assets/images/icons/linkedin_f.svg" alt=""></div>
                  <div class="footer-contact"><a href="<?= $linkedin ?>" target="_blank">Follow us on LinkedIn</a></div>
                </div>
                <?php
                }
                ?>

              </div>
            </div>
            <div class="col-12 col-xl-2">
              <div class="footer-border last">
                <div class="footer-contact-wrap">
                  <div class="footer-contact">
                    <p class="copy-right-txt">Copyright © Ganad <?= date('Y'); ?>.<br><br>Powered by <a class="ml-0" href="https://innovixdigital.com/" target="_blank">Innovix Digital.</a></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer><script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.slim.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.13.1/highlight.min.js"></script>
      <script src="<?php bloginfo('template_url'); ?>/plugin/plugin.min.js"></script>
      <script src="<?php bloginfo('template_url'); ?>/js/script.js"></script>
      <script src="<?php bloginfo('template_url'); ?>/js/jquery.multi-select.js"></script>
      <link rel="stylesheet"  href="<?php bloginfo('template_url') ?>/style/example-styles.css">
      <script src="https://kit.fontawesome.com/0fdd460947.js" crossorigin="anonymous"></script>
      <script src='https://www.google.com/recaptcha/api.js' async defer></script>

    </main>
  </body>
</html>