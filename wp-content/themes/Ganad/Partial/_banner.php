<?php
  $query = new WP_Query('pagename=media-network');
  if($query->have_posts()){
    while ($query->have_posts()) {
      $query->the_post();
      $banner_video = get_field("banner_video");
      $banner_video_mobile = get_field("banner_video_mobile");
      $page_title = get_field("page_title");
    }
    
  }
    
  $query1 = new WP_Query('pagename=contact');
  if($query1->have_posts()){
    while ($query1->have_posts()) {
      $query1->the_post();
      $facebook = get_field("facebook");
      $instagram = get_field("instagram");
      $twitter = get_field("twitter");
      $youtube = get_field("youtube");
      $linkedin = get_field("linkedin");
    }
    
  }

?>

<section class="banner-section bg-img">
  <video class="hero-vdo vd-large" loop="" muted="" autoplay>
    <source src="<?= $banner_video['url']; ?>" type="video/mp4">
  </video>

  <video class="hero-vdo vd-small" loop="" muted="" autoplay>
    <source src="<?= $banner_video_mobile['url']; ?>" type="video/mp4">
  </video>

  <div class="overlay bg-overlay"></div>
  <div class="banner-wrap">
    <div class="banner-header">
      <h1 class="header upper"><?= $page_title ?><a class="go-to" href="#homeSection-1">
          <div class="arrow go-to-btn"><span></span><span></span></div></a></h1>
    </div>

    <div class="banner-social">
      <div class="icon-wrap">
        <?php
          if (!empty($facebook)) {
        ?>
        <a href="<?= $facebook ?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
        <?php 
        } 
        if (!empty($instagram)) {
        ?>
        <a href="<?= $instagram ?>" target="_blank"><i class="fab fa-instagram"></i></a>
        <?php 
        } 
        if (!empty($twitter)) {
        ?>
        <a href="<?= $twitter ?>" target="_blank"><i class="fab fa-twitter"></i></a>
        <?php 
        } 
        if (!empty($youtube)) {
        ?>
        <a href="<?= $youtube ?>" target="_blank"><i class="fa fa-youtube-play"></i></a>
        <?php 
        } 
        if (!empty($linkedin)) {
        ?>
        <a href="<?= $linkedin ?>" target="_blank"><i class="fa fa-linkedin-square"></i></a>
        <?php 
        } 
        ?>
      </div>
    </div>
    
  </div>
</section>