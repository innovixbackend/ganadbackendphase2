<?php 
    if(have_posts()):
      while(have_posts()):
        the_post();
        $images = acf_photo_gallery('our_client', $post->ID);
?>
<?php  if( count($images) ): ?>
<section class="ourclient">
  <div class="container"> 
    <h5 class="title"><?php echo __("our_client", "ganad")  ?></h5>
    <div class="client-slide-wrap">
      <div id="client_slider_res"> 
      <?php 
          foreach($images as $image):
      ?>
          <div class="column-img"> 
            <img class="img-fluid" src="<?= $image['full_image_url']; ?>" alt="">
          </div>
      <?php 
          endforeach;
      ?>
      </div>
    </div>
  </div>
</section>
<?php
endif;
    endwhile;
    endif;
    get_footer();
?>