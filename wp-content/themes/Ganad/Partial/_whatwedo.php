<?php 

if(have_posts()):
  while(have_posts()):
    the_post(); 
?>
  <div class="col-12">
    <h3 class="sub-header upper pageheader"><?= get_field("page_title") ?></h3>
  </div>
  <div class="col-12 pagedesc"><?= nl2br(get_field("page_description")) ?></div>
<?php
    endwhile;
    endif;    
?>