<?php 
        $number = 1;
       
        $args = [
          'post_type' => 'services',
          'posts_per_page' => -1,
        ];
        $services=new WP_Query($args);
        if($services->have_posts()){
          while($services->have_posts()) {
            $services->the_post();
            $code = get_field("category_code");
            $zoomlevel = get_field("zoom_level");
            $wpageId = get_the_ID();
            $choose_detail_page = get_field('choose_detail_page');
            $detailurl="#";
            if($choose_detail_page){
              $detailurl = get_permalink($choose_detail_page->ID)."?code=".$code."&wpageId=".$wpageId."&zoomlevel=".$zoomlevel;
            }

            $count1 = 1;
            $active1 = "";
    
            $count2 = 1;
            $active2 = "";
    
            $count3= 1;
            $active3 = "";
    
            $desk_img = acf_photo_gallery('service_desktop_image', $post->ID);
            $popup_image = acf_photo_gallery('service_popup_image', $post->ID);
?>
<div class="col-md-6 col-lg-4 mb-5">
  <div class="card service-card" >
    <div class="card-overlay" data-toggle="modal" data-target="#imageModal-<?= $number?>"></div>
    <div class="slide service-carousel" data-interval="1000">
      <div class="carousel-inner">
      <?php 
          foreach($desk_img as $img1):
          if($count1==1){
            $active1 = "active";
          }
          else{
            $active1 = "";
          }


      ?>
        <div class="carousel-item <?= $active1  ?>"><img class="w-100" src="<?= $img1['full_image_url']; ?>" alt="Slide Image"></div>
      <?php 
          $count1 ++;
          endforeach;
      ?>
      </div>
    </div>
    
    <a class="card-title-link" href="<?= $detailurl ?>"> 
      <div class="card-body service-title">
        <h4 class="card-title">
          <span> <?= get_field("page_info_title") ?><i class="fa fa-external-link-square"></i></span>
        </h4>
      </div>
    </a>

  </div>
</div>
<div class="modal fade" id="imageModal-<?= $number?>" tabindex="-1" role="dialog" aria-labelledby="imageModal-<?= $number?>" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered cu-modal" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
        <div class="carousel slide" id="imageCarousel-<?= $number?>" data-ride="carousel">
          <div class="carousel-inner">
          <?php 
            foreach($popup_image as $img2):
            if($count2==1){
              $active2 = "active";
            }
            else{
              $active2 = "";
            }


          ?>
            <div class="carousel-item <?= $active2  ?>"><img class="img-fluid" src="<?= $img2['full_image_url']; ?>" alt="Slide Image"></div>
            <?php 
                $count2 ++;
                endforeach;
            ?>
          </div>
          <ol class="carousel-indicators modal-slide-thumbnail">
          <?php
            foreach($popup_image as $img3):
            if($count3==1){
              $active3 = "active";
            }
            else{
              $active3 = "";
            }
            $slide=$count3-1;

          ?>
            <li class="<?= $active3  ?>" data-target="#imageCarousel-<?= $number?>" data-slide-to="<?= $slide  ?>"><img class="img-fluid" src="<?= $img3['full_image_url']; ?>" alt="Slide Image"></li>
            <?php 
                $count3 ++;

                endforeach;
            ?>
          </ol>
        </div>
      </div>
    </div>
  </div>
</div>
<?php 
  $number++;
    }
  }
?>