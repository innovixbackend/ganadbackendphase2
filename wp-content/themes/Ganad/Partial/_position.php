<?php 
  $args = [
    'post_type' => 'careers',
    'posts_per_page' => -1,
  ];
  $careers=new WP_Query($args);
  if($careers->have_posts()){
    while($careers->have_posts()) {
      $careers->the_post();

?>
<div class="col-12">
  <div class="career-list">
    <h5 class="title"><?= get_field("job_title") ?></h5>
    <div class="ul-wrap">

      <ul>        
        <li>
          <label class="bold"><?php echo __("date", "ganad")  ?></label><span class="data"><?= get_field("job_date") ?></span>
        </li>
        <li class="dept">
          <label class="bold"><?php echo __("department", "ganad")  ?></label><span class="data"><?= get_field("department") ?></span>
        </li>
        <li>
          <label class="bold"><?php echo __("type", "ganad")  ?></label><span class="data"><?= get_field("type") ?> </span>
        </li>
      </ul><a class="cu-btn btn" href="<?=the_permalink() ?>"><?php echo __("read_more", "ganad")  ?></a>

    </div>
  </div>
</div>
<?php 
    }
  }
?>