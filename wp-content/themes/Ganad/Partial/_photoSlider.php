<section class="home-firsection section-0" id="homeSection-1">
	<style>
		.home-firsection .firstsec-slider .slick-prev {
			background-image: url(<?php bloginfo('template_url'); ?>/assets/images/icons/next1.svg);
		}
		.home-firsection .firstsec-slider .slick-next {
			background-image: url(<?php bloginfo('template_url'); ?>/assets/images/icons/next1.svg);
		}
	</style>
    <div class="firstsec-slider"> 
      <?php 
        $args = [
          'post_type' => 'photo_slider',
          'posts_per_page' => -1,
        ];
        $photo_slider=new WP_Query($args);
        if($photo_slider->have_posts()){
          while($photo_slider->have_posts()) {
            $photo_slider->the_post();
            $photoslider = get_field("photo_slider_desktop_image");
      ?>
        <div class="first-sec mt-0">
          <div class="firstsec-img">
            <div class="overlay"></div><img class="img-fluid img-desk" src="<?= $photoslider['url'] ?>" alt=""><img class="img-fluid img-mb" src="<?= GetImage("photo_slider_mobile_image","large") ?>" alt="">
            <div class="slide-desc-wrap">
              <div class="slide-desc">
                <h2><?= get_field("photo_slider_title") ?></h2>
                <p><?= get_field("photo_slider_description") ?></p>
              </div>
            </div>
          </div>
        </div>        
        <?php 
          }
        }
      ?>
    </div>
</section>