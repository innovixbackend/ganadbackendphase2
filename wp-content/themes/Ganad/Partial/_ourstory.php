<?php 
  if(have_posts()):
    while(have_posts()):
      the_post();        
?>
<section class="ourstory"> 
  <div class="container"> 
    <h5 class="title"><?php echo __("our_story", "ganad")  ?></h5>
    <div class="story_desc"> 
      <p><?= get_field("our_story") ?></p>
      
    </div>
    <div class="vision_mission">
      <div class="row">
        <div class="col-12">
          <div class="corporate_visio">
          <div class="vision_img wow fadeInRight" data-wow-delay=".5s" data-wow-duration="1s" style="visibility: visible; animation-delay: 5s; animation-name: fadeIn;"> 
              <div class="bg-color"></div><img src="<?php bloginfo('template_url'); ?>/assets/images/icons/growth.svg" alt="">
            </div>
            <div class="vision_txt">
              <label class="title"><?php echo __("mission", "ganad")  ?></label>
              <p><?= get_field("mission") ?></p>
            </div>
          </div>
          <div class="corporate_visio">
          <div class="vision_img wow fadeInRight" data-wow-delay=".5s" data-wow-duration="1s" style="visibility: visible; animation-delay: 5s; animation-name: fadeIn;">
              <div class="bg-color"></div><img src="<?php bloginfo('template_url'); ?>/assets/images/icons/vision.svg" alt="">
            </div>
            <div class="vision_txt">
              <label class="title"><?php echo __("vision", "ganad")  ?></label>
              <p><?= get_field("vision") ?></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php
    endwhile;
    endif;
 


?>