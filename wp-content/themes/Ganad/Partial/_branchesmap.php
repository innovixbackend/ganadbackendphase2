<?php
  $count =1;
  $selected = "false";
  $selectid = "map-tap";
  $tap ="map";
  $active = "";
  
  $textcount =1;
  $textshow ="";
  $textactive ="";

?>

<section class="branchandmap-wrapper">
  <div class="container">
    <h5 class="title"><?php echo __("branches_map", "ganad")  ?></h5>
    <div class="allbrandandwrap">
      <div class="tabheader">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
          <?php
            $args = [
              'post_type' => 'branchesandmap',
              'posts_per_page' => -1,
            ];
            $map=new WP_Query($args);
            if($map->have_posts()){
              while($map->have_posts()) {
                $map->the_post();
                if($count ==1){
                  $active = "active";
                  $selected = "true";
                }
                else{
                  $active = "";
                  $selected = "false";
                }
         ?>
          <li class="nav-item">
            <a class="nav-link <?= $active ?>" id="<?= $selectid ?>-<?= $count ?>" data-toggle="tab" href="#<?= $tap ?>-<?= $count ?>" role="tab" aria-controls="<?= $tap ?>-<?= $count ?>" aria-selected="<?=$selected ?>"> 
              <img class="img-fluid flag" src="<?= GetImage("flag","large") ?>" alt="">
              <span><?= get_field("city") ?></span>
            </a>
          </li>
          <?php
              $count ++;
              }
            }
          ?>     
        </ul>
      </div>
      <div class="barnch-wrap">
        <div class="tab-content" id="myTabContent">
        <?php
            $args = [
              'post_type' => 'branchesandmap',
              'posts_per_page' => -1,
            ];
            $map=new WP_Query($args);
            if($map->have_posts()){
              while($map->have_posts()) {
                $map->the_post();
                if($textcount ==1){
                  $textactive = "active";
                  $textshow = "show";
                }
                else{
                  $textactive = "";
                  $textshow = "";
                }
         ?>

          <div class="tab-pane fade <?= $textactive ?> <?= $textshow ?>" id="<?= $tap ?>-<?= $textcount ?>" role="tabpanel" aria-labelledby="<?= $selectid ?>-<?= $textcount ?>">
            <div class="tab-wrap">
              <div class="branchmap">
                <img class="img-fluid" src="<?= GetImage("branches_map","large") ?>" alt="">
              </div>
              <div class="branchtext">
                <h5 class="title"><?= get_field("branches_name") ?></h5>
                <p><?= nl2br(get_field("branches_description")) ?></p>
              </div>
            </div>
          </div>
          <?php
              $textcount ++;
              }
            }
          ?>  
        </div>
      </div>
    </div>
  </div>
</section>