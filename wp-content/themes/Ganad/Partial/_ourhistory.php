<section class="ourhistory-section">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <h5 class="title historytitle"><?php echo __("history", "ganad")  ?></h5>
              <div class="history-wrap">
                <ul>
                  <?php 
                    $row_num = 1 ;

                    $row_even = "slideInRight";
                    
                    $args = [
                      'post_type' => 'ourhistory',
                      'posts_per_page' => -1,
                    ];
                    $ourhistory=new WP_Query($args);
                    if($ourhistory->have_posts()){
                      while($ourhistory->have_posts()) {
                        $ourhistory->the_post();
                    if($row_num%2==0){
                      $row_even="slideInLeft";
                    }else{
                      $row_even="slideInRight";
                    }                  
                  ?>
                  <li class="text-center col-md-6 wow fadeIn" data-wow-delay=".5s" data-wow-duration="1s" style="visibility: visible; animation-delay: 5s; animation-name: fadeIn;">
                    <div class="content">
                    <span style="display:block;"><?= get_field("history_description") ?></span>
                      
                    </div>
                    <div class="year">
                      <div class="onhover">
                          <div class="hover-bg"></div>
                      </div>
                      <span><?= get_field("year") ?></span>
                    </div>
                  </li>
                  <?php 
                      $row_num++;
                      }
                    }
                  ?>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>