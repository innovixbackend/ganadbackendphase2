<section class="home-section-slider section-0">
        <div class="highlight-item">
        
        <?php 
          $args = [
            'post_type' => 'sevicehightlight',
            'posts_per_page' => -1,
          ];
          $sevicehightlight=new WP_Query($args);
          if($sevicehightlight->have_posts()){
            while($sevicehightlight->have_posts()) {
              $sevicehightlight->the_post();
              
              $choose_detail_page = get_field('choose_detail_page');

              //from Service Page
              $detailurl="#";
              if($choose_detail_page){
              $code = get_field("category_code",$choose_detail_page->ID);
              $zoomlevel = get_field("zoom_level",$choose_detail_page->ID);
              $wpageId = $choose_detail_page->ID;
              $productpage = get_field("choose_detail_page",$choose_detail_page->ID);
                if($productpage)
                {
                  $productpageID = $productpage->ID;
                  $detailurl = get_permalink($productpage->ID)."?code=".$code."&wpageId=".$wpageId."&zoomlevel=".$zoomlevel;
                }
              }
        ?>
          <a class="hightlight-wrap" href="<?= $detailurl ?>">
            <img class="hightlight-img img-fluid dsk-img" src="<?= GetImage("highlight_desktop_image","large") ?>" alt=""><img class="hightlight-img img-fluid mb-img" src="<?= GetImage("highlight_mobile_image","large") ?>" alt="">
            <div class="hightlight-text">
              <div class="text-wrap">
                <ul class="text-wrap-ul">
                  <li class="highlight-title first-name upper pageheader"><?= get_field("page_info_title") ?></li>
                  <li class="highlight-title hover-active mb-label upper pageheader"><?= get_field("page_info_title") ?></li>
                </ul>
              </div>
            </div>
        </a>
        <?php 
            }
          }
        ?>    
            
        </div>
      </section>