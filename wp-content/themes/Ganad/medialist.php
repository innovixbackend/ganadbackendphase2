<?php

add_action('admin_head', 'product_page_style');
add_action("wp_ajax_add_image", "add_image");
add_action("wp_ajax_delete_image", "delete_image");
add_action("wp_ajax_add_imagedetail", "add_imagedetail");
add_action("wp_ajax_delete_imagedetail", "delete_imagedetail");
add_action("wp_ajax_delete_sitelist", "delete_sitelist");
add_action("wp_ajax_unpublish_Product", "unpublish_Product");
add_action("wp_ajax_publish_Product", "publish_Product");
$GLOBALS['nonce'] = wp_create_nonce("my_user_vote_nonce");

function product_page_style() {
  if($_GET['page'] == "medialist"){
    echo "<style>";
    require_once("SiteList/style.css");
    echo "</style>";
  }  
}

add_action('admin_menu', 'addAdminPageContent');
function addAdminPageContent() {
  add_menu_page('Media List', 'Media List', 'edit_posts' ,'medialist', 'crudAdminPage', 'dashicons-tide');
}

function crudAdminPage() {
  global $wpdb;
  $table_name = 'sitelist';
  productProcess($wpdb,$table_name);

  if(!isset($_GET['process'])){
    $process = "listing";
  }else{
    $process = $_GET['process'];
  }

  switch($process){
    case "listing"    : require_once("SiteList/listing.php");break;
    case "show"       : require_once("SiteList/show.php");break;
    case "importForm" : require_once("SiteList/insert.php");break;
    case "editForm"   : require_once("SiteList/editForm.php");break;
    case "addImage"   : require_once("SiteList/addImage.php");break;
    case "addImageDetail"   : require_once("SiteList/addImageDetail.php");break;
  }

?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<?php  
} 

function getProcessUrl($process){
  $uri = $_SERVER['REQUEST_URI'];
  $current_url = explode("?", $uri);
  return $current_url[0]."?page=medialist&process=".$process;
}

function productProcess($wpdb ,$table_name){

  //Insert
  if($_GET['process'] == "insertProcess"){
    $count =1;
    $fileName = $_FILES["file"]["tmp_name"];
    $path_info = pathinfo($_FILES['file']['name']);
    $ext = $path_info['extension'];
    if($ext == "csv" ) {
      if ($_FILES["file"]["size"] > 0){
        $file = fopen($fileName, "r");
        while (($column = fgetcsv($file, 10000, ",")) !== FALSE){
          //skip title
          if($count !=1){
            $SNo = Validation($column[0]);
            $Name = Validation($column[1]);
            $CategoryCode = Validation($column[2]);
            $ServicesCategory = Validation($column[3]);
            $MediaCode = Validation($column[4]);
            $MediaType = Validation($column[5]);
            $ActualWidth = Validation($column[6]);
            $ActualHeight = Validation($column[7]);
            $TrafficMovement = Validation($column[8]);
            $City = Validation($column[9]);
            $StateAndDivision = Validation($column[10]);
            $Location = Validation($column[11]);
            $Longitude = Validation($column[12]);
            $Latitude = Validation($column[13]);
            $FilterVersion = Validation($column[14]);
			if (empty($MediaCode) && $MediaCode !== '0') {
              continue;
            }
            $result = $wpdb->get_results("SELECT * FROM $table_name WHERE MediaCode='$MediaCode'");
            if(!empty($result))
            {
              $__update = 'UPDATE '.$table_name.' SET SNo="'.$SNo.'",Name="'.$Name.'",CategoryCode="'.$CategoryCode.'",ServicesCategory="'.$ServicesCategory.'",MediaType="'.$MediaType.'",ActualWidth="'.$ActualWidth.'",ActualHeight="'.$ActualHeight.'",TrafficMovement="'.$TrafficMovement.'",City="'.$City.'",StateAndDivision="'.$StateAndDivision.'",Location="'.$Location.'",Longitude="'.$Longitude.'",Latitude="'.$Latitude.'",FilterVersion="'.$FilterVersion.'" WHERE MediaCode="'.$MediaCode.'"';
              $wpdb->query($__update);
              
            }
            else{
              $__insert = 'INSERT INTO '.$table_name.'(SNo,Name,CategoryCode,ServicesCategory,MediaCode,MediaType,ActualWidth,ActualHeight,TrafficMovement,City,StateAndDivision,Location,Longitude,Latitude,FilterVersion) VALUES("'.$SNo.'","'.$Name.'","'.$CategoryCode.'","'.$ServicesCategory.'","'.$MediaCode.'","'.$MediaType.'","'.$ActualWidth.'","'.$ActualHeight.'","'.$TrafficMovement.'","'.$City.'","'.$StateAndDivision.'","'.$Location.'","'.$Longitude.'","'.$Latitude.'","'.$FilterVersion.'")';
              $wpdb->query($__insert);
            }

          }
          
        $count++;
        }
      }
      // var_dump($wpdb->show_errors());
      // var_dump($wpdb->print_error());
      header('Location: '.getProcessUrl('listing'));
      exit; 
    }
    else{
     
     ?>
     <script>
        alert("Not a vilid csv file!");
        location.replace('admin.php?page=medialist&process=importForm');
       </script>
     <?php
    }
  }


  //Update
  if($_GET['process'] == "updateProcess"){
    if (isset($_GET['SId'])){
      $SId = Validation($_GET['SId']);
      $Name = Validation($_POST['Name']);
      $CategoryCode = Validation($_POST['CategoryCode']);
      $ServicesCategory = Validation($_POST['ServicesCategory']);
      $MediaType = Validation($_POST['MediaType']);
      $ActualWidth = Validation($_POST['ActualWidth']);
      $ActualHeight = Validation($_POST['ActualHeight']);
      $TrafficMovement = Validation($_POST['TrafficMovement']);
      $City = Validation($_POST['City']);
      $StateAndDivision = Validation($_POST['StateAndDivision']);
      $Location = Validation($_POST['Location']);
      $Longitude = Validation($_POST['Longitude']);
      $Latitude = Validation($_POST['Latitude']);
      $FilterVersion = Validation($_POST['FilterVersion']);
      $YoutubeLink = Validation($_POST['YoutubeLink']);
      $MapUrl = Validation($_POST['MapUrl']);

      $update_result = $wpdb->get_results("SELECT * FROM $table_name WHERE SId='$SId'");

      if(!empty($update_result))
      {
        $__update = 'UPDATE '.$table_name.' SET Name="'.$Name.'",CategoryCode="'.$CategoryCode.'",ServicesCategory="'.$ServicesCategory.'",MediaType="'.$MediaType.'",ActualWidth="'.$ActualWidth.'",ActualHeight="'.$ActualHeight.'",TrafficMovement="'.$TrafficMovement.'",City="'.$City.'",StateAndDivision="'.$StateAndDivision.'",Location="'.$Location.'",Longitude="'.$Longitude.'",Latitude="'.$Latitude.'",FilterVersion="'.$FilterVersion.'",YoutubeLink="'.$YoutubeLink.'",MapUrl="'.$MapUrl.'" WHERE SId="'.$SId.'"';
        $wpdb->query($__update);
      }

      header('Location: '.getProcessUrl('listing'));
      exit;
    }
  }
}

function add_image(){
  $dir = get_template_directory()."/SiteList/SiteImages/";
  $num = rand(0, 1000);
  $filename =$num. $_FILES["file"]["name"];
  $path = $dir.$filename;
  $SId = $_POST["SId"];
  $dbpath = "\\\\SiteList\\\\SiteImages\\\\".$filename;
  move_uploaded_file($_FILES["file"]["tmp_name"], $path);
  global $wpdb;
  $__insertimg = 'INSERT INTO siteimages (SId,ImagePath) VALUES ("'.$SId.'","'.$dbpath.'")';
  $wpdb->query($__insertimg);
}

function delete_image(){
  $imageId = $_POST["imageId"];
  global $wpdb;
  $__deleteimg ='DELETE FROM siteimages Where ImageId="'.$imageId.'"';
  $wpdb->query($__deleteimg);
}

function add_imagedetail(){
  $dir = get_template_directory()."/SiteList/SiteImagesDetail/";
  $num = rand(0, 1000);
  $filename =$num. $_FILES["file"]["name"];
  $path = $dir.$filename;
  $SId = $_POST["SId"];
  $dbpath = "\\\\SiteList\\\\SiteImagesDetail\\\\".$filename;
  move_uploaded_file($_FILES["file"]["tmp_name"], $path);
  global $wpdb;
  $__insertimg = 'INSERT INTO detailimages (SId,ImagePath) VALUES ("'.$SId.'","'.$dbpath.'")';
  $wpdb->query($__insertimg);
}

function delete_imagedetail(){
  $imageId = $_POST["imageId"];
  global $wpdb;
  $__deleteimg ='DELETE FROM detailimages Where DetailImageId="'.$imageId.'"';
  $wpdb->query($__deleteimg);
}

function delete_sitelist(){
  $siteId = $_POST["siteId"];
  global $wpdb;
  $__deletesitelist ='DELETE FROM sitelist Where SId="'.$siteId.'"';
  $wpdb->query($__deletesitelist);
  $__deleteimglist ='DELETE FROM siteimages Where SId="'.$siteId.'"';
  $wpdb->query($__deleteimglist);
  $__deleteimgdetail ='DELETE FROM detailimages Where SId="'.$siteId.'"';
  $wpdb->query($__deleteimgdetail);
}

function unpublish_Product(){
  $siteId = $_POST["siteId"];
  global $wpdb;
  $__unpublishProduct ='UPDATE sitelist set IsPublish="UnPublish" Where SId="'.$siteId.'"';
  $wpdb->query($__unpublishProduct);
 
}

function publish_Product(){
  $siteId = $_POST["siteId"];
  global $wpdb;
  $__unpublishProduct ='UPDATE sitelist set IsPublish="" Where SId="'.$siteId.'"';
  $wpdb->query($__unpublishProduct);
 
}
?>