<?php

function SMTPSetting(){
	$logo ="";
	$adminemail ="";
	$host = host();

	//Site Setting Information
  $query = new WP_Query('pagename=site-setting');
    if($query->have_posts()){
        while ($query->have_posts()) {
        $query->the_post();
 
        $logo = GetImage("logo","large");
				$adminemail = get_field("admin_email");
				$careeremail = get_field("career_email");
        } 
	 }

	 //Contact Information
	 $contact = new WP_Query('pagename=contact');
    if($contact->have_posts()){
        while ($contact->have_posts()) {
        $contact->the_post();
 
				$address = get_field("address");
        $phone_number_1 = get_field("phone_number_1");
        $phone_number_2 = get_field("phone_number_2");
        } 
	 }
	return [
		'username' => "noreply.innovix.smtp@gmail.com",
		'password' => "Inn0viX@2020",
		'logo' => $logo,
		'adminemail' => $adminemail,
		'careeremail' => $careeremail,
		'host' => $host,
		'address' => $address,
		'phone_number_1' => $phone_number_1,
		'phone_number_2' => $phone_number_2
	];
}

function contactemail($category,$mediaCode,$name, $jobposition, $email, $phone,$company,$companysize,$country,$inquiry)
{
	$categorydata ="";
	$mediaCodedata ="";
	if(!empty($category)){
			$categorydata ="<tr>
			<td width='20%' style='padding-bottom: 10px'>
					<span style='font-weight:bold;color:#001841;'>Product Name </span>
				</td>
				<td width='5%' style='padding-bottom: 10px'>
				:
				</td>
				<td style='padding-bottom: 10px'>
					<span>".$category. "</span>
				</td>
			</tr>";
		}
		if(!empty($mediaCode)){
			$mediaCodedata ="<tr>
			<td width='20%' style='padding-bottom: 10px'>
					<span style='font-weight:bold;color:#001841;'>Media Code</span>
				</td>
				<td width='5%' style='padding-bottom: 10px'>
				:
				</td>
				<td style='padding-bottom: 10px'>
					<span>".$mediaCode. "</span>
				</td>
			</tr>";
		}

		$setting = SMTPSetting();
    $result=false;
    include_once "phpmailer/PHPMailerAutoload.php";
    $mail = new PHPMailer(true);

    //From email address and name
    $mail->From = "noreply@ganad.com.my";
    $mail->FromName = "Ganad Enquiry";



	$mail->addAddress($setting['adminemail']);
    //$mail->addAddress('david@supersevenstars.com'); // Add a recipien
    //$mail->addCC('hninsusan@kia.com.mm');
    //$mail->addBCC('susu@kia.com.mm');
    $mail->addReplyTo('noreply.innovix.smtp@gmail.com');

    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
    $mail->isHTML(true); // Set email format to HTML

    $mail->Subject = 'Contact Us for Ganad Enquiry';
    // $mail->Body    = '<h2>This is the HTML message body <b>in bold!</b><h2></br> <h3>'.$name.'</h3>';
    $mail->Body = "<!DOCTYPE html>
    <html lang='en'>
    <head> </head>
    <body style='font-family:Roboto-Regular,sans-serif;font-size:14px;line-height: 25px;'>
        <table align='center'>
            <tr>
                <td align='center' >
                    <table width='600' cellpadding='10' cellspacing='0' style='border:1px solid #cbcccd;'>
                        <tr>
                            <td align='left' width='120px'><img
                                    src='http://gd.innovix-solutions.net/wp-content/uploads/2020/08/logo.png' style='width:80px;'>
                            </td>
							<td>
								<p style='font-weight:bold;color:#001841;font-size: 22px;margin: 0;margin-left: 10px;'>Myanmar Ganad Advertising Co., Ltd</p>
							</td>
                        </tr>
						<tr>
							<td colspan='2'>
																<table style='width:100%'>
									".$categorydata."
									<tr>
										<td width='20%' style='padding-bottom: 10px'>
											<span style='font-weight:bold;color:#001841;'>Name </span>
										</td>
										<td width='5%' style='padding-bottom: 10px'>
										:
										</td>
										<td style='padding-bottom: 10px'>
											<span>".$name. "</span>
										</td>
									</tr>
									".$mediaCodedata."
									<tr>
										<td width='20%' style='padding-bottom: 10px'>
											<span style='font-weight:bold;color:#001841;'>Job Position </span>
										</td>
										<td width='5%' style='padding-bottom: 10px'>
										:
										</td>
										<td style='padding-bottom: 10px'>
											<span>".$jobposition. "</span>
										</td>
                                    </tr>
										<tr>
											<td width='20%' style='padding-bottom: 10px'>
												<span style='font-weight:bold;color:#001841;'>Email </span>
											</td>
											<td width='5%' style='padding-bottom: 10px'>
											:
											</td>
											<td style='padding-bottom: 10px'>
												<span>".$email. "</span>
											</td>
										</tr>               
                                    <tr>
										<td width='20%' style='padding-bottom: 10px'>
											<span style='font-weight:bold;color:#001841;'>Phone </span>
										</td>
										<td width='5%' style='padding-bottom: 10px'>
										:
										</td>
										<td style='padding-bottom: 10px'>
											<span>".$phone. "</span>
										</td>
                                    </tr>
                                    
                                    <tr>
										<td width='20%' style='padding-bottom: 10px'>
											<span style='font-weight:bold;color:#001841;'>Company </span>
										</td>
										<td width='5%' style='padding-bottom: 10px'>
										:
										</td>
										<td style='padding-bottom: 10px'>
											<span>".$company. "</span>
										</td>
                                    </tr>

                                    <tr>
										<td width='20%' style='padding-bottom: 10px'>
											<span style='font-weight:bold;color:#001841;'>Company Size </span>
										</td>
										<td width='5%' style='padding-bottom: 10px'>
										:
										</td>
										<td style='padding-bottom: 10px'>
											<span>".$companysize. "</span>
										</td>
                                    </tr>

                                    <tr>
										<td width='20%' style='padding-bottom: 10px'>
											<span style='font-weight:bold;color:#001841;'>Country </span>
										</td>
										<td width='5%' style='padding-bottom: 10px'>
										:
										</td>
										<td style='padding-bottom: 10px'>
											<span>".$country. "</span>
										</td>
                                    </tr>

									<tr>
										<td width='20%' valign='top' style='vertical-align:top;'>
										<span style='font-weight:bold;color:#001841;'>Inquiry </span>

										</td>
										<td width='5%' valign='top' style='vertical-align:top;'>
										:
										</td>
										<td valign='top' style='vertical-align:top;'>
											<span>".$inquiry. "</span>
										</td>
									</tr>
                                </table>
                            </td>
						</tr>
                        <tr>
                            <td style='background-color:#333;color:#ffffff;text-align:center;' align='center' colspan='2'>".$setting['address']. "
							<br>
							<a style='color:#ffffff;' href='tel:".$setting['phone_number_1']. "'>
                            ".$setting['phone_number_1']. "
							</a>
							<br>
						    <a href='".$setting['host']."' style='color:#ffffff;' target='_blank'>".$setting['host']."</a>
							</td>
                        </tr>
                    </table>
                <td>
            <tr>
        </table>
    </body>
    
    </html> ";
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    if ($mail->send()) {
      $result = true;
    } 
    return $result;
}

function applyform($jobtitle,$name, $phone, $email, $applyposition,$attachfile)
{
		$setting = SMTPSetting();
    $result=false;
    include_once "phpmailer/PHPMailerAutoload.php";
    $mail = new PHPMailer(true);

    //From email address and name
    $mail->From = "noreply@ganad.com.my";
    $mail->FromName = "Job Application Form";
    
    
	$mail->addAddress($setting['careeremail']);
    //$mail->addAddress('david@supersevenstars.com'); // Add a recipien
    //$mail->addCC('hninsusan@kia.com.mm');
    //$mail->addBCC('susu@kia.com.mm');
   $mail->addReplyTo('noreply.innovix.smtp@gmail.com');

    $mail->addAttachment($attachfile);         // Add attachments
    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
    $mail->isHTML(true); // Set email format to HTML

    $mail->Subject = 'Job Apply Form for Ganad';
    // $mail->Body    = '<h2>This is the HTML message body <b>in bold!</b><h2></br> <h3>'.$name.'</h3>';
    $mail->Body = "<!DOCTYPE html>
    <html lang='en'>
    <head> </head>
    <body style='font-family:Roboto-Regular,sans-serif;font-size:14px;line-height: 25px;'>
        <table align='center'>
            <tr>
                <td align='center' >
                    <table width='600' cellpadding='10' cellspacing='0' style='border:1px solid #cbcccd;'>
                        <tr>
                            <td align='left' width='120px'><img
                                    src='http://gd.innovix-solutions.net/wp-content/uploads/2020/08/logo.png' style='width:80px;'>
                            </td>
							<td>
								<p style='font-weight:bold;color:#001841;font-size: 22px;margin: 0;margin-left: 10px;'>Myanmar Ganad Advertising Co., Ltd</p>
							</td>
                        </tr>
						<tr>
							<td colspan='2'>
								<table style='width:100%'>
									<tr>
										<td width='20%' style='padding-bottom: 10px'>
											<span style='font-weight:bold;color:#001841;'>Job Title </span>
										</td>
										<td width='5%' style='padding-bottom: 10px'>
										:
										</td>
										<td style='padding-bottom: 10px'>
											<span>".$jobtitle. "</span>
										</td>
									</tr>
									<tr>
										<td width='20%' style='padding-bottom: 10px'>
											<span style='font-weight:bold;color:#001841;'>Name </span>
										</td>
										<td width='5%' style='padding-bottom: 10px'>
										:
										</td>
										<td style='padding-bottom: 10px'>
											<span>".$name. "</span>
										</td>
									</tr>
									
                  <tr>
										<td width='20%' style='padding-bottom: 10px'>
											<span style='font-weight:bold;color:#001841;'>Phone </span>
										</td>
										<td width='5%' style='padding-bottom: 10px'>
										:
										</td>
										<td style='padding-bottom: 10px'>
											<span>".$phone. "</span>
										</td>
									</tr>
									
									<tr>
										<td width='20%' style='padding-bottom: 10px'>
											<span style='font-weight:bold;color:#001841;'>Email </span>
										</td>
										<td width='5%' style='padding-bottom: 10px'>
										:
										</td>
										<td style='padding-bottom: 10px'>
											<span>".$email. "</span>
										</td>
									</tr>
									
									<tr>
										<td width='20%' style='padding-bottom: 10px'>
											<span style='font-weight:bold;color:#001841;'>Apply Position </span>
										</td>
										<td width='5%' style='padding-bottom: 10px'>
										:
										</td>
										<td style='padding-bottom: 10px'>
											<span>".$applyposition. "</span>
										</td>
									</tr>
									
									
                                   
                                </table>
                            </td>
						</tr>
                        <tr>
                            <td style='background-color:#333;color:#ffffff;text-align:center;' align='center' colspan='2'>".$setting['address']. "
							<br>
							<a style='color:#ffffff;' href='tel:".$setting['phone_number_1']. "'>
                            ".$setting['phone_number_1']. "
							</a>
							<br>
							<a href='".$setting['host']."' style='color:#ffffff;' target='_blank'>".$setting['host']."</a>
							</td>
                        </tr>
                    </table>
                <td>
            <tr>
        </table>
    </body>
    
    </html> ";
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    if ($mail->send()) {
      $result = true;
    } 
    return $result;
}
?>