<?php
  $query = new WP_Query('pagename=site-setting');
  if($query->have_posts()){
      while ($query->have_posts()) {
      $query->the_post();

      $logo = GetImage("logo","large");
      $adminemail = get_field("admin_email");
      } 
 }

 
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php GetSEO(); ?>
    <title>Ganad</title>
    <link rel="icon" href="<?php bloginfo('template_url'); ?>/assets/images/icons/favicon-logo.png">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/plugin/plugin.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/plugin/plugin.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style/style.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style/modified.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style/pagination.css">

    <link href="https://fonts.googleapis.com/css2?family=Barlow:wght@500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Barlow:wght@500&family=Poppins:wght@600;700&display=swap" rel="stylesheet">
	
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-160754881-3"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-160754881-3');
	</script>

  </head>
  <body>
    <main>
      <header>
        <nav class="header-nav">
          <div class="container">
            <div class="row">
              <div class="col-12">


                <div class="header-nav-wrap">
                  <ul class="img-ul">
                  <li><a href="<?= home_url('./') ?>"><img src="<?= $logo ?>" alt=""></a></li>
                  </ul>
                  <ul class="img flag">
                    <li><a href="#" target="_blank"><img src="<?php bloginfo('template_url'); ?>/assets/images/icons/malaysiaflag.svg" alt=""></a></li>
                    <li><a class="active"><img src="<?php bloginfo('template_url'); ?>/assets/images/icons/myanmarflag.svg" alt=""></a></li>
                  </ul>
                  <div class="header-menu-wrap">
                    <ul class="menu-ul">
                    <?php 
                      $args = [
                        'post_type' => 'menu',
                        'posts_per_page' => -1,
                      ];
                      $menu=new WP_Query($args);
                      if($menu->have_posts()){
                        while($menu->have_posts()) {
                          $menu->the_post();
                          $menu_link = get_field("menu_link");
                          $active ="";
                          if (!empty($menu_link)) {
                            $post = get_post(get_the_ID()); 
                            $slug = $post->post_name;
                            $menutitle = get_field("menu_title");
                            // if($slug == "media-network"){
                            //   $menu_link = projectfolder().'\mm\\';
                            // }

                            //check active class
                            if($slug == "media-network" && is_home(''))
                            {
                              $active ="active";
                            }
                            
                            if($slug == "services" && is_page('service'))
                            {
                              $active ="active";
                            }
                            
                            if($slug == "our-work" && is_page('our-work'))
                            {
                              $active ="active";
                            }

                            else if($slug == "about-us" && is_page('aboutus'))
                            {
                              $active ="active";
                            }

                            else if($slug == "careers" && (is_page('career') || is_singular('careers') ))
                            {
                              $active ="active";
                            }

                            else if($slug == "contact-us" && is_page('contact'))
                            {
                              $active ="active";
                            }

                      //check service
                      if($slug == "services")
                      {

                        $servivetitle = get_field("menu_title");
                    ?>
                      <li class="dropdown <?= $active ?>"><a class="upper" href="<?= $menu_link ?>"><?= get_field("menu_title")?></a>
                        <a class="btn dropdown-toggle" id="dropdownMenuButton" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-angle-down"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <div class="shape">
                          <?php
                        
                              $args = [
                                'post_type' => 'burgermenu',
                                'posts_per_page' => -1,
                              ];
                              $burgermenu=new WP_Query($args);
                              if($burgermenu->have_posts()){
                                while($burgermenu->have_posts()) {
                                  $burgermenu->the_post();
                                  $burger_image = get_field("burger_image");
                                  $choose_detail_page = get_field('choose_detail_page');

                                  //from Service Page
                                  $detailurl="#";
                                  if($choose_detail_page){
                                  $code = get_field("category_code",$choose_detail_page->ID);
                                  $zoomlevel = get_field("zoom_level",$choose_detail_page->ID);
                                  $wpageId = $choose_detail_page->ID;
                                  $productpage = get_field("choose_detail_page",$choose_detail_page->ID);
                                    if($productpage)
                                    {
                                      $productpageID = $productpage->ID;
                                      $detailurl = get_permalink($productpage->ID)."?code=".$code."&wpageId=".$wpageId."&zoomlevel=".$zoomlevel;
                                    }
                                  }
                            ?>
                            <a class="dropdown-item" href="<?= $detailurl ?>"><?= get_field("page_info_title") ?></a>
                            <?php
                              }
                            }
                            ?>
                          </div>
                        </div>
                      </li>
                      <li class="mb-servies-menu">
                        <div class="update-mb-menu d-flex">
                          <a class="upper" href="<?= $menu_link ?>"> <span <?= $active ?>><?=  $menutitle?></span></a>
                          <div id="main"><a class="upper" href="javascript:void(0)" onclick="openNav()"> <i class="fas fa-chevron-right"></i></a></div>
                        </div>
                      </li>
                      <?php
                          }
                          // end service

                          //start normal menu
                          else{
                      ?>
                      <li class="<?= $active ?>"><a class="upper" href="<?= $menu_link ?>"><?= get_field("menu_title")?></a></li>
                    <?php 
                          }
                          //normal menu
                          }
                        }
                      }
                    ?>
                    </ul>

                    <!-- Start Service Mobile Menu -->
                    <div class="sidenav" id="mySidenav">
                      <div class="container">
                        <div class="mb-menu-link">
                          <div class="sec-mb-menu">
                            <a class="closebtn upper" href="javascript:void(0)" onclick="closeNav()"> <i class="fas fa-chevron-left"></i>
                              <span><?= $servivetitle?></span>
                            </a>
                          </div>

                          <?php
                        
                        $args = [
                          'post_type' => 'burgermenu',
                          'posts_per_page' => -1,
                        ];
                        $burgermenu=new WP_Query($args);
                        if($burgermenu->have_posts()){
                          while($burgermenu->have_posts()) {
                            $burgermenu->the_post();
                            $burger_image = get_field("burger_image");
                            $choose_detail_page = get_field('choose_detail_page');

                            //from Service Page
                            $detailurl="#";
                            if($choose_detail_page){
                            $code = get_field("category_code",$choose_detail_page->ID);
                            $zoomlevel = get_field("zoom_level",$choose_detail_page->ID);
                            $wpageId = $choose_detail_page->ID;
                            $productpage = get_field("choose_detail_page",$choose_detail_page->ID);
                              if($productpage)
                              {
                                $productpageID = $productpage->ID;
                                $detailurl = get_permalink($productpage->ID)."?code=".$code."&wpageId=".$wpageId."&zoomlevel=".$zoomlevel;
                              }
                            }
                      ?>

                          <div class="sec-mb-menu">
                            <a class="upper" href="<?= $detailurl ?>"><?= get_field("page_info_title") ?></a>
                          </div>
                          <?php
                         }
                        }
                        ?>
                            
                        </div>
                      </div>
                    </div>
                    <!-- End Service Mobile Menu -->

                    <!-- Start Burger Menu Image -->
                    <div class="menu-ul header-feature-img">
                      <div class="row">
                      
                      <?php
                        
                        $args = [
                          'post_type' => 'burgermenu',
                          'posts_per_page' => -1,
                        ];
                        $burgermenu=new WP_Query($args);
                        if($burgermenu->have_posts()){
                          while($burgermenu->have_posts()) {
                            $burgermenu->the_post();
                            $burger_image = get_field("burger_image");
                            $choose_detail_page = get_field('choose_detail_page');

                            //from Service Page
                            $detailurl="#";
                            if($choose_detail_page){
                            $code = get_field("category_code",$choose_detail_page->ID);
                            $zoomlevel = get_field("zoom_level",$choose_detail_page->ID);
                            $wpageId = $choose_detail_page->ID;
                            $productpage = get_field("choose_detail_page",$choose_detail_page->ID);
                              if($productpage)
                              {
                                $productpageID = $productpage->ID;
                                $detailurl = get_permalink($productpage->ID)."?code=".$code."&wpageId=".$wpageId."&zoomlevel=".$zoomlevel;
                              }
                            }
                      ?>
                        <div class="col-4 feature-col">
                          <a href="<?= $detailurl ?>">
                            <div class="feature-img"> 
                              <img class="w-100" src="<?= $burger_image['url'] ?>" alt="Slide Image">
                              <div class="caption">
                                <p class="upper"><?= get_field("page_info_title") ?></p>
                              </div>
                            </div>
                          </a>
                        </div>
                        <?php
                         }
                        }
                        ?>
                            
                      </div>
                    </div>
                    <!-- End Burger Menu Image -->

                  </div>
                  <div class="menu-btn">
                    <div class="menu-icon"></div>
                  </div>
                </div>



              </div>
              <div class="col-12">
                <div class="copy-right">
                  <p class="copy-right-txt">Copyright © Ganad <?= date('Y'); ?>. <span>
                       Powered by <a href="https://innovixdigital.com/" target="_blank">Innovix Digital</a></span></p>
                </div>
              </div>
            </div>
          </div>
        </nav>
      </header>
      <div class="preloader-wrapper">
        <div class="preloader"><img src="<?php bloginfo('template_url'); ?>/assets/images/icons/loading.gif" alt="loading"></div>
      </div>