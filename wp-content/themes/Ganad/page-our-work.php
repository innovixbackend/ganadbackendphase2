<?php
    get_template_part('otherheader');
    
    if(have_posts()):
    while(have_posts()):
        the_post(); 
        $page_title = get_field("page_title");
        endwhile;
    endif;
     
?>
 <section class="our-work"> 
  <div class="container">
    <h3 class="sub-header pageheader"><?= $page_title ?></h3>
    
      <?php 
        $row_num = 1 ;
        $row_reverse = "";
        $args = [
          'post_type' => 'ourwork',
          'posts_per_page' => 5,
          'paged' => get_query_var( 'paged' )
        ];
        $ourwork=new WP_Query($args);
        if($ourwork->have_posts()){
          while($ourwork->have_posts()) {
            $ourwork->the_post();
            $youtube_link = get_field("youtube_link");
            $count1 = 1 ;
            $active1 = "active";

            $count2 = 1;
            $active1 = "active";

            $work_images = acf_photo_gallery('work_images', $post->ID);
            if($row_num%2==0){
              $row_reverse="row-reverse";
            }else{
              $row_reverse="";
            }
      ?>
      <div class="work-section">

      <div class="row <?= $row_reverse ?>">
        <div class="col-md-5 work-col"> 
        <?php
          if (empty($youtube_link)) {
        ?>
          <div class="custom-card" data-toggle="modal" data-target="#imageModal-<?= $row_num?>">
            <?php
              foreach($work_images as $img1){

            ?>
            <img class="img-fluid" src="<?= $img1['full_image_url']; ?>" alt="">
            <?php
              break;
              }
            ?>
            <div class="search-icn"> <img src="<?php bloginfo('template_url'); ?>/assets/images/icons/plus.svg" alt=""></div>
          </div>
          <div class="modal fade" id="imageModal-<?= $row_num?>" tabindex="-1" role="dialog" aria-labelledby="imageModal-<?= $row_num?>" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered cu-modal" role="document">
              <div class="modal-content">
                <div class="modal-body">
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
                  <div class="carousel slide" id="imageCarousel-<?= $row_num?>" data-ride="carousel">
                    <div class="carousel-inner">
                    <?php
                      foreach($work_images as $img1){
                      if($count1==1){
                        $active1 = "active";
                      }
                      else{
                        $active1 = "";
                      }

                    ?>
                      <div class="carousel-item <?=$active1 ?> "><img class="img-fluid" src="<?= $img1['full_image_url']; ?>" alt="Slide Image"></div>
                      <?php
                        $count1++;
                        }
                      ?>
                    </div>
                    <ol class="carousel-indicators modal-slide-thumbnail">
                    <?php
                      foreach($work_images as $img1){
                        if($count2==1){
                          $active2 = "active";
                        }
                        else{
                          $active2 = "";
                        }
                        $slide=$count2-1;
    
                    ?>
                      <li class="<?= $active2 ?>" data-target="#imageCarousel-<?= $row_num?>" data-slide-to="<?= $slide ?>"><img class="img-fluid" src="<?= $img1['full_image_url']; ?>" alt="Slide Image"></li>
                      <?php
                       $count2++;
                      }
                    ?>  
                    </ol>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php 
        } 
        else {
        ?>
          <div class="custom-card" data-toggle="modal" data-target="#imageModal-<?= $row_num?>">
          <?php
              foreach($work_images as $img1){

            ?>
            <img class="img-fluid" src="<?= $img1['full_image_url']; ?>" alt="">
            <?php
              break;
              }
            ?>
              <div class="youtube-logo"> <img src="<?php bloginfo('template_url'); ?>/assets/images/icons/youtubeplay-button.png" alt=""></div>
          </div>
          <div class="modal modal-youtube fade videopopup" id="imageModal-<?= $row_num?>" tabindex="-1" role="dialog" aria-labelledby="imageModal-<?= $row_num?>" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered cu-modal" role="document">
              <div class="modal-content">
                <div class="modal-body">
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
                  <div class="carousel slide" id="imageCarousel-<?= $row_num?>" data-ride="carousel">
                    <iframe width="100%" height="500px" src="https://www.youtube.com/embed/wV3N-wCRL2k"></iframe>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php 
        } 
        ?>
        </div>
        <div class="col-md-6 work-col">
          <div class="work-txt career-detail">
            <div class="work-wrap">
              <h5 class="title"><?= get_field("work_title") ?></h5>
              <div class="date-type"> 
                <ul class="type"> 
                  <li> <strong class="type-label bold"><?php echo __("client", "ganad")  ?> </strong><span><?= get_field("client") ?></span></li>
                  <li> <strong class="type-label bold"><?php echo __("media_assets", "ganad")  ?> </strong><span><?= get_field("media_assets") ?></span></li>
                  <li> <strong class="type-label bold"><?php echo __("durations", "ganad")  ?> </strong><span><?= get_field("duration") ?></span></li>
                  <li> <strong class="type-label bold"><?php echo __("location", "ganad")  ?> </strong><span><?= get_field("location") ?></span></li>
                  <li> 
                    <p><?= get_field("work_description") ?></p>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    
   <?php 
      $row_num++;
          }
        }

    ?>

<!-- pagination -->
    <div class="row">
      <div class="col-12">
          <div class="pagination-wrap">
          <?php 
          
        $big = 999999999; // need an unlikely integer
        echo paginate_links( array(
            'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
            'format' => '?paged=%#%',
            'current' => max( 1, get_query_var('paged') ),
            'total' => $ourwork->max_num_pages
        ) );
          
          ?>
          </div>
      </div>
  </div>  
  </div>
</section>   
<?php
    get_footer();
 
?>