<?php
session_start();
$path = current_location();
if(isset($_SESSION['path'])){
  $path = $_SESSION['path'];
}
?>
<div class="customtabel">
<div class="topheader">
    <img src="<?php bloginfo('template_url'); ?>/SiteList/Icons/add.svg" alt="">
    <h6 class="pageheader">Upload Product</h6>
  </div>
  <div class="listing" >
    <div class="row">
      <div class="col-6">
        <div class="imgbtn-wrapper">
          <form action="<?= getProcessUrl('insertProcess') ?>" method="post" accept-charset="utf-8"  enctype="multipart/form-data">
            <div class="form-group">
              <input type="file" class="form-control-file" name="file" required="">
            </div>

            <div>
              <button type="submit" id="newsubmit" name="newsubmit" class="btn btn-default btn-sm btndesign">Save</button>
              <a href="<?= $path ?>" class="btn btn-sm btndesign" >Back</a>
            </div>
           
          </form>
        </div>
        
      </div>
      
    </div>
    
  </div>
</div>