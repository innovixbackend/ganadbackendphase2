<?php
session_start();
$path = current_location();
if(isset($_SESSION['path'])){
  $path = $_SESSION['path'];
}
if($_GET['process'] == "editForm"){
  if (isset($_GET['SId']))
  {
    $SId = $_GET['SId'];
    $result = $wpdb->get_results("SELECT * FROM $table_name WHERE SId='$SId'");
    foreach($result as $print) {
      $MediaCode = $print->MediaCode;
      $Name = $print->Name;
      $CategoryCode = $print->CategoryCode;
      $ServicesCategory =  $print->ServicesCategory;
      $MediaType = $print->MediaType;
      $TrafficMovement = $print->TrafficMovement;
      $ActualWidth = $print->ActualWidth;
      $ActualHeight = $print->ActualHeight;
      $City = $print->City;
      $StateAndDivision = $print->StateAndDivision;
      $Location = $print->Location;
      $Longitude = $print->Longitude;
      $Latitude = $print->Latitude;
      $FilterVersion = $print->FilterVersion;
      $ImageUrl = $print->ImageUrl;
      $YoutubeLink = $print->YoutubeLink;
      $MapUrl = $print->MapUrl;
    }
  
?>
<div class="customtabel" >
  <div class="topheader">
    <img src="<?php bloginfo('template_url'); ?>/SiteList/Icons/update.svg" alt="">
    <h6 class="pageheader">Update</h6>
  </div>
  <div class="updatetable">
    <div class="row">
      <div class="col-6 offset-3">
          <div class="form-wrap">
              <form action="<?= getProcessUrl('updateProcess') ?>&SId=<?=$SId ?>" method="POST">
                <div class="form-group">
                  <label for="formGroupExampleInput">Media Code</label>
                  <input class="form-control" type="text" name="MediaCode" value="<?= $MediaCode ?>" disabled>
                </div>
                <div class="form-group">
                  <label for="formGroupExampleInput">Name</label>
                  <input class="form-control" type="text" name="Name" value="<?= $Name ?>" autocomplete="off" required="">
                </div>
                <div class="form-group">
                  <label for="formGroupExampleInput2">Services Category Code</label>
                  <input class="form-control" type="text" name="CategoryCode" value="<?= $CategoryCode ?>" autocomplete="off" required="">
                </div>
                <div class="form-group">
                  <label for="formGroupExampleInput2">Services Category</label>
                  <input class="form-control" type="text" name="ServicesCategory" value="<?= $ServicesCategory ?>" autocomplete="off" required="">
                </div>
                <div class="form-group">
                  <label for="formGroupExampleInput2">Media Type</label>
                  <input class="form-control" type="text" name="MediaType" value="<?= $MediaType ?>" autocomplete="off" required="">
                </div>
                <div class="form-group">
                  <label for="formGroupExampleInput2">Actual Width</label>
                  <input class="form-control" type="number" name="ActualWidth" value="<?= $ActualWidth ?>" autocomplete="off" required=""  step="0.01">
                </div>
                <div class="form-group">
                  <label for="formGroupExampleInput2">Actual Height</label>
                  <input class="form-control" type="number" name="ActualHeight" value="<?= $ActualHeight ?>" autocomplete="off" required="" step="0.01">
                </div>
                <div class="form-group">
                  <label for="formGroupExampleInput2">Traffic Movement</label>
                  <input class="form-control" type="text" name="TrafficMovement" value="<?= $TrafficMovement ?>" autocomplete="off" required="">
                </div>
                <div class="form-group">
                  <label for="formGroupExampleInput2">City</label>
                  <input class="form-control" type="text" name="City" value="<?= $City ?>" autocomplete="off" required="">
                </div>
                <div class="form-group">
                  <label for="formGroupExampleInput2">State / Division</label>
                  <input class="form-control" type="text" name="StateAndDivision" value="<?= $StateAndDivision ?>" autocomplete="off" required="">
                </div>
                <div class="form-group">
                  <label for="exampleFormControlTextarea1">Location</label>
                  <textarea class="form-control" name="Location" rows="3" required=""><?= $Location ?></textarea>
                </div>
                <div class="form-group">
                  <label for="formGroupExampleInput2">Longitude</label>
                  <input class="form-control" type="number" name="Longitude" value="<?= $Longitude ?>" autocomplete="off" required="" step="any">
                </div>
                <div class="form-group">
                  <label for="formGroupExampleInput2">Latitude</label>
                  <input class="form-control" type="number" name="Latitude" value="<?= $Latitude ?>" autocomplete="off" required="" step="any">
                </div>
                <div class="form-group">
                  <label for="formGroupExampleInput2">FilterVersion</label>
                  <input class="form-control" type="text" name="FilterVersion" value="<?= $FilterVersion ?>" autocomplete="off" required="">
                </div>
                <div class="form-group">
                  <label for="formGroupExampleInput2">YoutubeLink</label>
                  <input class="form-control" type="text" name="YoutubeLink" value="<?= $YoutubeLink ?>" autocomplete="off">
                </div>
                <div class="form-group">
                  <label for="formGroupExampleInput2">MapUrl</label>
                  <input class="form-control" type="text" name="MapUrl" value="<?= $MapUrl ?>" autocomplete="off">
                </div>
                <div class="form-group">
                  <button class="btn btn-sm btndesign" type="submit" name="submit" value="submit">Submit</button>
                  <a href="<?= $path ?>" class="btn btn-sm btndesign" >Back</a>
                </div>
              </form>
          </div>
      </div>

  </div>
</div>

<?php
    }
  }
?>