<?php
session_start();
$_SESSION['path'] = current_location();
  $limit = 15;
  $page=1;
  $startPage =0; 
  $endPage =0;
  $maxPages = 5;
  if (isset($_GET["mediacode"])){
    $mediacode  = trim($_GET["mediacode"]); 
  }
  if (isset($_GET["servicelist"])){
    $servicelist  = $_GET["servicelist"]; 
  }

?>
<div class="customtabel">
  <div class="topheader">
    <img src="<?php bloginfo('template_url'); ?>/SiteList/Icons/list.svg" alt="">
    <h6 class="pageheader">Media List</h6>
  </div>
 
  <div class="listing listingpage" >
    <form action="admin.php" method="Get">
      <input type="hidden" name="page" value="medialist">
      <div class="row">
        <div class="col-3">
          <div class="form-group">
            <input type="text" class="form-control" name="mediacode" placeholder="Media Code" autocomplete="off" value="<?=$mediacode ?>">
          </div>
        </div>
      
        <div class="col-3">
          <div class="form-group">
            <select class="form-control servicelist" id="servicelist" name="servicelist">
            <option value="">Choose Service</option>
              <?php
                $args = [
                  'post_type' => 'services',
                  'posts_per_page' => -1,
                ];
                $services=new WP_Query($args);
                if($services->have_posts()){
                  while($services->have_posts()) {
                    $services->the_post();
              ?>
              <option value="<?= get_field("service_title") ?>" <?= (get_field("service_title") ==  $servicelist) ? 'selected' : ''; ?>><?= get_field("service_title") ?></option>
              <?php 
                  }
                }
              ?>
            </select>
          </div>
        </div>
        <div class="col-3 topbarbtn">
          <button type="submit" class="btn btn-default btn-sm btndesign" >Search</button>
          <a href="<?= getProcessUrl('importForm') ?>" class="btn btn-default btn-sm btndesign" >Create</a>
        </div>
      </div>
      </form> 
      <table class="table table-hover table-bordered">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Media Code</th>
            <th scope="col">Media Type</th>
            <th scope="col">Services</th>
            <th scope="col">City</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
        <?php

        if (isset($_GET["pageNo"])) {
          $page  = $_GET["pageNo"]; 
        }
       

        $start_from = ($page-1) * $limit; 
        $__data ="SELECT * FROM $table_name WHERE (MediaCode like '%$mediacode%' OR '$mediacode' IS NULL OR '$mediacode' = '') AND (ServicesCategory='$servicelist' OR '$servicelist' IS NULL OR '$servicelist' = '') ORDER BY SId ASC LIMIT $start_from, $limit";

        $result = $wpdb->get_results($__data);
        $no = 1;
        foreach ($result as $print) {
          ?>
          <tr>
            <td><?= $no + (($page-1)* $limit) ?></td>
            <td class="strong"><?= $print->MediaCode ?></td>
            <td><?= $print->MediaType ?></td>
            <td><?= $print->ServicesCategory ?></td>
            <td><?= $print->City ?></td>
            <td>
              <input type="hidden" id="siteId" value="<?=$print->SId ?>">
              <a href="<?= getProcessUrl('show') ?>&SId=<?=$print->SId ?>" class="btn btn-default btn-sm btndesign">View</a>
              <a href="<?= getProcessUrl('editForm') ?>&SId=<?=$print->SId ?>" class="btn btn-default btn-sm btndesign">Update</a>
              <button id="deleteproduct"  onclick="deletePro(<?=$print->SId ?>)" class="btn btn-default btn-sm btndesign">Delete</button>
              <a href="<?= getProcessUrl('addImage') ?>&SId=<?=$print->SId ?>" class="btn btn-default btn-sm btndesign">Listing Image</a>
              <a href="<?= getProcessUrl('addImageDetail') ?>&SId=<?=$print->SId ?>" class="btn btn-default btn-sm btndesign">Detail Image</a>
              <?php
                if(empty($print->IsPublish))
                {
              ?>
                <button id="unpublishProduct"  onclick="unpublishProduct(<?=$print->SId ?>)" class="btn btn-default btn-sm btndesign">UnPublish</button>
              <?php
                }
                else{
              ?>
              <button id="publishProduct"  onclick="publishProduct(<?=$print->SId ?>)" class="btn btn-danger btn-sm btndesign">Published</button>
              <?php
                }
              ?>
              
            </td>
          </tr>
          <?php
          $no++;
          }
          ?>
        </tbody>
      </table>

      <?php
         $__pagination ="SELECT COUNT(*) as total FROM $table_name WHERE (MediaCode='$mediacode' OR '$mediacode' IS NULL OR '$mediacode' = '') AND (ServicesCategory='$servicelist' OR '$servicelist' IS NULL OR '$servicelist' = '')";
         $pagi_result = $wpdb->get_results($__pagination);
         $total_records = $pagi_result[0]->total;  
         $totalPages = ceil($total_records / $limit); 

         if($totalPages  >1 )
         {

        
          if ($page < 1)
          {
              $page = 1;
          }
          else if ($page > $totalPages)
          {
              $page = $totalPages;
          }
          if ($totalPages <= $maxPages)
          {
              // total pages less than max so show all pages
              $startPage = 1;
              $endPage = $totalPages;
          }
          else
          {
              // total pages more than max so calculate start and end pages
              $maxPagesBeforeCurrentPage = (int)($maxPages / 2);
              $maxPagesAfterCurrentPage = ceil($maxPages /2) - 1;

              if ($page <= $maxPagesBeforeCurrentPage)
              {
                  // current page near the start
                  $startPage = 1;
                  $endPage = $maxPages;
              }
              else if ($page + $maxPagesAfterCurrentPage >= $totalPages)
              {
                  // current page near the end
                  $startPage = $totalPages - $maxPages + 1;
                  $endPage = $totalPages;
              }
              else
              {
                  // current page somewhere in the middle
                  $startPage = $page - $maxPagesBeforeCurrentPage;
                  $endPage = $page + $maxPagesAfterCurrentPage;
              }
          }
      ?>
      <!-- pagination -->
      <nav aria-label="...">
        <ul class="pagination">
          <?php
          //Start First
          if ($page > 1)
            {
          ?>
          <li class="page-item">
            <a class="page-link" href="admin.php?page=medialist&pageNo=1&mediacode=<?= $mediacode ?>&servicelist=<?= $servicelist ?>" >First</a>
          </li>
          <?php
            }
            if ($page > 1){
          ?>
          <li class="page-item">
            <a class="page-link" href="admin.php?page=medialist&pageNo=<?= $page-1 ?>&mediacode=<?= $mediacode ?>&servicelist=<?= $servicelist ?>"><</a>
          </li>
          <?php
            }
            //End First

             //Start Pagination Number
            for ($i = $startPage; $i <= $endPage; $i++)
            {

              if ($page == $i)
                {
                  $active = "active";
                }
                else
                {
                  $active = "";
                }
          ?>

          <li class="page-item  <?=  $active ?>">
            <a class="page-link" href="admin.php?page=medialist&pageNo=<?= $i ?>&mediacode=<?= $mediacode ?>&servicelist=<?= $servicelist ?>"><?=$i ?></a>
          </li>

          <?php
            }
            //End Pagination Number

            //Start Last
            if ($page < $totalPages)
            {
          ?>
          <li class="page-item">
            <a class="page-link" href="admin.php?page=medialist&pageNo=<?= $page+1 ?>&mediacode=<?= $mediacode ?>&servicelist=<?= $servicelist ?>">></a>
          </li>
          <?php
            }
            if ($page < $totalPages){
          ?>

          <li class="page-item">
            <a class="page-link" href="admin.php?page=medialist&pageNo=<?= $totalPages ?>&mediacode=<?= $mediacode ?>&servicelist=<?= $servicelist ?>">Last</a>
          </li>
          <?php
            }
            //End Last
          ?>

        </ul>
      </nav>
      <?php
         }
      ?>
  </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>
function deletePro(siteId) {
    var data= new FormData();
    data.append('siteId',siteId);
    data.append('action', 'delete_sitelist');
    data.append('nonce', '<?= $GLOBALS['nonce'] ?>');
    if (confirm("Do you want to delete?") == true) 
    {
      $.ajax({
        url: "<?= admin_url( 'admin-ajax.php' ) ?>", // Url to which the request is send
        type: "POST",             // Type of request to be send, called as method
        data: data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
        contentType: false,       // The content type used when sending data to the server.
        cache: false,             // To unable request pages to be cached
        processData:false,        // To send DOMDocument or non processed data file it is set to false
        success: function(data)   // A function to be called if request succeeds
        {
          alert("Delete Successfully");
          window.location.reload();
        }

      });
    }  
   
}

function unpublishProduct(siteId) {
    var data= new FormData();
    data.append('siteId',siteId);
    data.append('action', 'unpublish_Product');
    data.append('nonce', '<?= $GLOBALS['nonce'] ?>');
    if (confirm("Do you want to unpublish product?") == true) 
    {
      $.ajax({
        url: "<?= admin_url( 'admin-ajax.php' ) ?>", // Url to which the request is send
        type: "POST",             // Type of request to be send, called as method
        data: data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
        contentType: false,       // The content type used when sending data to the server.
        cache: false,             // To unable request pages to be cached
        processData:false,        // To send DOMDocument or non processed data file it is set to false
        success: function(data)   // A function to be called if request succeeds
        {
          alert("UnPublish Product Successfully.");
          window.location.reload();
        }

      });
    }  
   
}


function publishProduct(siteId) {
    var data= new FormData();
    data.append('siteId',siteId);
    data.append('action', 'publish_Product');
    data.append('nonce', '<?= $GLOBALS['nonce'] ?>');
    if (confirm("Do you want to publish?") == true) 
    {
      $.ajax({
        url: "<?= admin_url( 'admin-ajax.php' ) ?>", // Url to which the request is send
        type: "POST",             // Type of request to be send, called as method
        data: data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
        contentType: false,       // The content type used when sending data to the server.
        cache: false,             // To unable request pages to be cached
        processData:false,        // To send DOMDocument or non processed data file it is set to false
        success: function(data)   // A function to be called if request succeeds
        {
          alert("Published Product Successfully.");
          window.location.reload();
        }

      });
    }  
   
}
</script>
   
