<?php
session_start();
$path = current_location();
if(isset($_SESSION['path'])){
  $path = $_SESSION['path'];
}

if($_GET['process'] == "show"){
  if (isset($_GET['SId']))
  {
    $SId = $_GET['SId'];
    $result = $wpdb->get_results("SELECT * FROM $table_name WHERE SId='$SId'");
    foreach($result as $print) {
      $MediaCode = $print->MediaCode;
      $MediaType = $print->MediaType;
      $TrafficMovement = $print->TrafficMovement;
      $ActualWidth = $print->ActualWidth;
      $ActualHeight = $print->ActualHeight;
      $City = $print->City;
      $StateAndDivision = $print->StateAndDivision;
      $Location = $print->Location;
      $ImageUrl = $print->ImageUrl;
    }
    
   
    $imgcount = 0;
    $olactive = "";
    $coralcount = 0;
    $coralactive = "";

    // $defaultimg = "/SiteList/SiteImages/12product.jpg";
    $defaultimg = "/SiteList/DefaultImg/default1.jpg";
    $images = $wpdb->get_results("SELECT * FROM siteimages WHERE SId='$SId'");
?>
<div class="customtabel" >
  <div class="topheader">
    <img src="<?php bloginfo('template_url'); ?>/SiteList/Icons/eye.svg" alt="">
    <h6 class="pageheader">Detail</h6>
  </div>
  <div class="listing" >
    <div class="detail-table">
      <div class="row">
        <div class="col-5">
          <!-- <img class="img-fluid" src="<?php bloginfo('template_url'); ?><?= $ImageUrl ?>"> -->
          <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
              <?php
                if(!empty($images))
                {
                  foreach($images as $img){

                    if($imgcount == 0)
                    {
                      $olactive = "active";
                    }
                    else{
                      $olactive = "";
                    }
                  
              ?>
              <li data-target="#carouselExampleIndicators" data-slide-to="<?= $imgcount ?>" class="<?= $olactive ?>"></li>
                <?php
                  $imgcount++;
                   }
                }
              ?>
             
            </ol>
            <div class="carousel-inner">
              <?php
                if(!empty($images))
                {
                  foreach($images as $img){

                    if($coralcount == 0)
                    {
                      $coralactive = "active";
                    }
                    else{
                      $coralactive = "";
                    }
              ?>
                <div class="carousel-item <?= $coralactive ?>">
                  <img class="d-block w-100" src="<?php bloginfo('template_url'); ?><?= $img->ImagePath ?>" alt="First slide">
                </div>
              <?php
                   $coralcount++;

                  }
                }

                //default image
                else{
                  ?>
                 <div class="carousel-item active">
                  <img class="d-block w-100" src="<?php bloginfo('template_url'); ?><?= $defaultimg ?>" alt="First slide">
                </div>
              <?php
                }
              ?>
            </div>
            <?php
            if(!empty($images)){
              if($imgcount > 1){
            ?>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
            <?php
              }
            }
            ?>
          </div>
        </div>
        <div class="col-7">
          <div class="data">
            <h6>Media Type</h5>
            <span>:</span>
            <p><?= $MediaType ?></p>
          </div>
          <div class="data">
            <h6>Media Code</h5>
            <span>:</span>
            <p><?= $MediaCode ?></p>
          </div>
          <div class="data">
            <h6>Traffic Movement</h5>
            <span>:</span>
            <p><?= $TrafficMovement ?></p>
          </div>
          <div class="data">
            <h6>Actual Width</h5>
            <span>:</span>
            <p><?= $ActualWidth ?></p>
          </div>
          <div class="data">
            <h6>Actual Height</h5>
            <span>:</span>
            <p><?= $ActualHeight ?></p>
          </div>
          <div class="data">
            <h6>City</h5>
            <span>:</span>
            <p><?= $City ?></p>
          </div>
          <div class="data">
            <h6>State / Division</h5>
            <span>:</span>
            <p><?= $StateAndDivision ?></p>
          </div>
          <div class="data">
            <h6>Location</h5>
            <span>:</span>
            <p><?= $Location ?></p>
          </div>
          <div class="backtolink">
            <a href="<?= $path ?>" class="btn btn-default btn-sm btndesign" >Back</a>
          </div>
        </div>
        
    </div>
    
    
  </div>
</div>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js">
<?php
    }
  }
?>



