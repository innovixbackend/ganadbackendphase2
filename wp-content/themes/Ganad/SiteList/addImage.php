<?php
session_start();
$path = current_location();
if(isset($_SESSION['path'])){
  $path = $_SESSION['path'];
}

if($_GET['process'] == "addImage"){
  if (isset($_GET['SId']))
  {
    $SId = $_GET['SId'];
    $target_dir = get_template_directory().projectname()."\SiteList\\";
    $__addimg = $wpdb->get_results("SELECT img.ImagePath,img.ImageId FROM siteimages as img inner join sitelist as slist on img.SId = slist.SId WHERE img.SId='$SId'");
?>
<div class="customtabel" >
  <div class="topheader">
    <img src="<?php bloginfo('template_url'); ?>/SiteList/Icons/addimg.svg" alt="">
    <h6 class="pageheader">Add Image</h6>
  </div>
  <div class="addimg-wrpper">
    <div class="addimg">
      <div class="row">
        <div class="col-6">
            <div class="imgbtn-wrapper">
            <form id="fileupload" method="post" enctype="multipart/form-data">
              <div class="btn btn-sm btndesign btn-image-upload">
                <span>Add Image</span>
                <input type="file" class="form-control-file" id="addimg">
              </div>
            </form>
            <a href="<?= $path ?>" class="btn btn-default btn-sm btndesign" >Back</a>
            </div>
        </div>
      </div>
    </div>
    <div class="showimg-wrapper">
      <div class="show-img-wrap">
        <div class="row">
          <?php
          if(!empty($__addimg)){
            foreach($__addimg as $img){

          ?>
          <div class="col-3 mb-5">
            <div class="det-showimg">
              <img class="img-fluid" src="<?php bloginfo('template_url'); ?><?=$img->ImagePath;?>">
              <img class="img-fluid closeicon" id="<?=$img->ImageId;?>"  src="<?php bloginfo('template_url'); ?>/SiteList/quit.svg">
            </div>
          </div>
          <?php
            }
          }
            
          ?>
        </div>
      </div>

    </div>
  </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<?php
  }
}


  function wpb_hook_javascript() {
    if($_GET['process'] == "addImage") { 
      ?>
         <script>
          $(document).ready(function (e) {

            //Add Image
            $("#addimg").on('change',(function(e) {
              e.preventDefault();
              var name= document.getElementById('addimg');
              var img=name.files[0];
              var data= new FormData();
              if(!!img.type.match(/image.*/))
              {
                data.append('file',img);
                data.append('action', 'add_image');
                data.append('SId', '<?= $_GET['SId']?>');
                data.append('nonce', '<?= $GLOBALS['nonce'] ?>');
                
                  $.ajax({
                  url: "<?= admin_url( 'admin-ajax.php' ) ?>", // Url to which the request is send
                  type: "POST",             // Type of request to be send, called as method
                  data: data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                  contentType: false,       // The content type used when sending data to the server.
                  cache: false,             // To unable request pages to be cached
                  processData:false,        // To send DOMDocument or non processed data file it is set to false
                  success: function(data)   // A function to be called if request succeeds
                  {
                    alert("Upload Successfully");
                    window.location.reload();
                  }
                  });
              }
              else{
              alert('Not a valid image!');
            }
            }));

            //Delete Image
            $(".closeicon").on("click", function()
            {
              if (confirm("Do you want to delete?") == true) {
                var imageId = $(this).attr('id');
                // alert(imageId);
                var data= new FormData();
                data.append('imageId',imageId);
                data.append('action', 'delete_image');
                data.append('nonce', '<?= $GLOBALS['nonce'] ?>');

                $.ajax({
                url: "<?= admin_url( 'admin-ajax.php' ) ?>", // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false,        // To send DOMDocument or non processed data file it is set to false
                success: function(data)   // A function to be called if request succeeds
                {
                  alert("Delete Successfully");
                  window.location.reload();
                }
                });

              }
              
            });

          });
      </script>
      <?php
    }
  }
  
  add_action('admin_footer', 'wpb_hook_javascript');


?>

