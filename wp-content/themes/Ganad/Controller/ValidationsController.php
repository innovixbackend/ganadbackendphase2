<?php
if(!function_exists('Validation'))
{
  function Validation($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }
}  
?>