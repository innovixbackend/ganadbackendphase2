<?php
if(!function_exists('current_location')){
  function current_location() {
    if (isset($_SERVER['HTTPS']) &&
      ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
      isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
      $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
      $protocol = 'https://';
    } else {
      $protocol = 'http://';
    }
    return $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
  }
}

if(!function_exists('host'))
{
  function host(){
    if (isset($_SERVER['HTTPS']) &&
        ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
        isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
        $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
        $protocol = 'https://';
      } else {
        $protocol = 'http://';
      }
      return $protocol . $_SERVER['HTTP_HOST'];
    }
}

if(!function_exists('projectfolder'))
{
  function projectfolder(){
    $projectname ="";
    $path = host().$projectname;
    return $path;
  }
}

if(!function_exists('projectname'))
{
  function projectname(){
    $projectname ="/Ganad";
    return $projectname;
  }
}

if(!function_exists('UrlwithoutPara'))
{
  function UrlwithoutPara(){
    $uri = $_SERVER['REQUEST_URI'];
    $current_url = explode("?", $uri);
    
    return $current_url[0];
  }
}

?>