<?php
    global $wpdb;
    $limit = 15;
    $page=1;
    $startPage =0; 
    $endPage =0;
    $maxPages = 5;

    $zoomlevel = 6;
    if (!empty($_GET['zoomlevel']) && $_GET["zoomlevel"] != ''){
      $zoomlevel =Validation($_GET['zoomlevel']);
    }

    $wpageId ="393";
    if (!empty($_GET['wpageId']) && $_GET["wpageId"] != ''){
      $wpageId =Validation($_GET['wpageId']);
    }

    $code ="SC001";
    if (!empty($_GET['code']) && $_GET["code"] != ''){
      $code =Validation($_GET['code']);
    }

    //Search
    $_smedia ="";
    $sqlmediaCode = "";
    $sqlname = "";
    $sqlcity = "";
    $publishdata ="And (IsPublish IS NULL OR IsPublish = '')";

      //MediaCode
    if (!empty($_GET["mediaCode"]) && $_GET["mediaCode"] != 'NULL') {
      $_mediaCode = $_GET['mediaCode'];

      foreach ($_mediaCode as $mCode){
        $_smedia .=",'".$mCode."'";
        $mediaarray .= "mediaCode[]=".$mCode."&"; 
      }
      $mediaarray = ltrim($mediaarray, 'mediaCode[]=');
      $mediaarray = rtrim($mediaarray, "&");

      $_smedia = ltrim($_smedia, ',');

      $sqlmediaCode = " And MediaCode IN ($_smedia)";
      $mediapara ="mediaCode[]";
    }
    
    //mediaType
    if (!empty($_GET["productname"])){
      $_name = Validation($_GET['productname']);
      $sqlname = " And Location like '%$_name%'";
    }
    
    //City
    if (!empty($_GET["city"])){
      $_city = Validation($_GET['city']);
      $sqlcity = " And City ='$_city'";
    }

    $searchbar ="SELECT * FROM sitelist WHERE CategoryCode='$code' $sqlmediaCode $sqlname $sqlcity $publishdata";
    $bar = $wpdb->get_results($searchbar);

    $filter ="SELECT * FROM sitelist WHERE CategoryCode='$code' $publishdata";
    $filterResult = $wpdb->get_results($filter);

    if(!empty($bar)){
      $count = 0;
    foreach ($bar as $print)
    {
        $Latitude = $print->Latitude;
        $Longitude = $print->Longitude;
        $MediaCode = $print->MediaCode;
        $MediaType = $print->MediaType;
        $Location = $print->City;
        $path = home_url('./product-detail') ."?productId=".$print->SId;
        $preJSON[$count] = array(
            "Latitude" => $Latitude,
            "Longitude" => $Longitude,
            "MediaCode" => $MediaCode,
            "MediaType" => $MediaType,
            "Location" => $Location,
            "Link"=>$path
        );

        $count++;
    }
    // $jsondata = wp_send_json( $preJSON );
    $jsondata = json_encode( $preJSON );
    }
    
    get_template_part('otherheader');
    $defaultimg = "";

?>

<section class="service-section service-wrapper-margin">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <h3 class="sub-header upper pageheader"><?= the_field('page_info_title', $wpageId) ?></h3>
            </div>
            <div class="col-12"><?= nl2br(the_field('highlight_description', $wpageId)) ?></div>
          </div>
        </div>
      </section>
      <section class="services-search pt-2 pb-2"> 
        <div class="container"> 
          <form action="" method="GET">
            <input type="hidden" value="<?=$code ?>" name="code">
            <input type="hidden" value="<?=$wpageId ?>" name="wpageId">
            <div class="search-section">
              <input type="text" Placeholder="Name" name="productname" value="<?= $_GET['productname'] ?>" autocomplete="off">
              <div class="city-section media-arrow">
                <select class="browser-default custom-select-form" multiple  id="mediaCode" name="mediaCode[]">
                  <?php
                      foreach ($filterResult as $print)
                      {
                  ?>
                  <option value="<?= $print->MediaCode ?>" <?= ((isset($_GET['mediaCode']) && in_array($print->MediaCode, $_GET['mediaCode']) ? 'selected' : '')); ?>><?= $print->MediaCode ?></option>
                  <?php
                      }
                  ?>
                </select>
                <i class="fas fa-angle-down"></i>
              </div>
              <div class="city-section city-1">
                <select class="browser-default custom-select-form" id="city" name="city">
                  <option value="">Location</option>
                  <?php
                      $citySeasons = [];
                      foreach ($filterResult as $print)
                      {
                        
                          if (!in_array($print->City, $citySeasons)) {
                              $citySeasons[] = $print->City;
                  ?>
                  <option value="<?= $print->City ?>" <?= ((isset($_GET['city']) && ($_GET['city'] == $print->City) ? 'selected' : '')); ?>><?= $print->City ?></option>
                  <?php
                          }
                      }
                  ?>
                </select>
                <i class="fas fa-angle-down"></i>
              </div>
              <button class="submit-btn btn upper" type="submit">search</button>
            </div>
          </form>
        </div>
      </section>
      <section class="service-section">
        <div class="container">
          <div class="row">
            <?php
          
        
            if (isset($_GET["pageNo"])) {
              $page  = $_GET["pageNo"]; 
            }
           
            $start_from = ($page-1) * $limit; 
        
            $__searchresult ="SELECT * FROM sitelist WHERE CategoryCode='$code' $sqlmediaCode $sqlname $sqlcity $publishdata ORDER BY SId ASC LIMIT $start_from, $limit";
            $result = $wpdb->get_results($__searchresult);
            $defaultimg = "/SiteList/DefaultImg/default1.jpg";
            foreach ($result as $print)
            {
                $__img ="SELECT * FROM siteimages WHERE SId='$print->SId'";
                $__imgresult = $wpdb->get_results($__img);
                $count1 = 1;
                $active1 = "";
            ?>

             <!-- Show Data -->
             <div class="col-md-6 col-lg-4 mb-5">
                <a href="<?= home_url('./product-detail') ?>?productId=<?=$print->SId ?>">
                    <div class="card service-card" data-toggle="modal" data-target="#imageModal-1">
                    
                    <?php if(empty($print->YoutubeLink)) 
                      {
                      ?>
                    <div class="card-overlay"></div>
                    <div class="slide service-carousel" data-interval="1000">
                        <?php
                           if(!empty($__imgresult))
                           {
                            foreach ($__imgresult as $img)
                              {
                                if($count1==1){
                                  $active1 = "active";
                                }
                                else{
                                  $active1 = "";
                                }
                    
                        ?>
                        <div class="carousel-item <?= $active1  ?>">
                          <img class="w-100" src="<?php bloginfo('template_url'); ?><?= $img->ImagePath ?>" alt="Slide Image">
                        </div>
                        <?php
                              $count1++;
                              }
                           }
                           else{
                        ?>
                        <div class="carousel-item active">
                          <img class="w-100" src="<?php bloginfo('template_url'); ?><?= $defaultimg ?>" alt="Slide Image">
                        </div>
                        <?php
                           }
                        ?>
                    </div>
                    <?php
                      }
                      else
                      {
                    ?>
                    <div class="slide service-carousel" data-interval="1000">
                        <div class="carousel-item active">
                        <iframe width="100%" height="375px" src="<?= $print->YoutubeLink ?>"></iframe>
                        </div>
                    </div>
                    <?php
                      }
                    ?>
                    <a class="card-title-link" href="<?= home_url('./product-detail') ?>?productId=<?=$print->SId ?>">
                      <div class="card-body service-title">
                          <h4 class="card-title"> <span><?= $print->MediaCode ?><i class="fa fa-external-link-square"></i></span></h4>
                      </div>
                    </a>
                    
                    </div>
                </a>
            </div>

            <?php
                }
            ?>
          </div>

          <!-- Pagination -->
          <div class="pagination_btn">
            <div class="pagination-section"> 
            <?php

              $__pagination ="SELECT COUNT(*) as total FROM sitelist WHERE CategoryCode='$code' $sqlmediaCode $sqlname $sqlcity";
              $pagi_result = $wpdb->get_results($__pagination);
              $total_records = $pagi_result[0]->total;  
              $totalPages = ceil($total_records / $limit); 


              if($totalPages  >1 )
              {


                if ($page < 1)
                {
                    $page = 1;
                }

                else if ($page > $totalPages)
                {
                    $page = $totalPages;
                }
                if ($totalPages <= $maxPages)
                {
                    // total pages less than max so show all pages
                    $startPage = 1;
                    $endPage = $totalPages;
                }
                else
                {
                    // total pages more than max so calculate start and end pages
                    $maxPagesBeforeCurrentPage = (int)($maxPages / 2);
                    $maxPagesAfterCurrentPage = ceil($maxPages /2) - 1;

                    if ($page <= $maxPagesBeforeCurrentPage)
                    {
                        // current page near the start
                        $startPage = 1;
                        $endPage = $maxPages;
                    }
                    else if ($page + $maxPagesAfterCurrentPage >= $totalPages)
                    {
                        // current page near the end
                        $startPage = $totalPages - $maxPages + 1;
                        $endPage = $totalPages;
                    }
                    else
                    {
                        // current page somewhere in the middle
                        $startPage = $page - $maxPagesBeforeCurrentPage;
                        $endPage = $page + $maxPagesAfterCurrentPage;
                    }
                }
            ?>
            <!-- pagination -->
            <nav aria-label="...">
              <ul class="pagination">
                <?php
                //Start First
                if ($page > 1)
                  {
                ?>
                <li class="page-item">
                  <a class="page-link" href="<?=UrlwithoutPara()?>?pageNo=1&<?=$mediapara ?>=<?= $mediaarray ?>&productname=<?=$_name ?>&city=<?= $_city ?>&code=<?= $code ?>&wpageId=<?= $wpageId ?>&zoomlevel=<?= $zoomlevel ?>" >First</a>
                </li>
                <?php
                  }
                  if ($page > 1){
                ?>
                <li class="page-item">
                  <a class="page-link" href="<?=UrlwithoutPara()?>?pageNo=<?= $page-1 ?>&<?=$mediapara ?>=<?= $mediaarray ?>&productname=<?=$_name ?>&city=<?= $_city ?>&code=<?= $code ?>&wpageId=<?= $wpageId ?>&zoomlevel=<?= $zoomlevel ?>"><</a>
                </li>
                <?php
                  }
                  //End First
                 
                  //Start Pagination Number
                  for ($i = $startPage; $i <= $endPage; $i++)
                  {


                    if ($page == $i)
                      {
                        $active = "active";
                      }
                      else
                      {
                        $active = "";
                      }
                ?>

                <li class="page-item  <?=  $active ?>">
                  <a class="page-link" href="<?=UrlwithoutPara()?>?pageNo=<?= $i ?>&<?=$mediapara ?>=<?= $mediaarray ?>&productname=<?=$_name ?>&city=<?= $_city ?>&code=<?= $code ?>&wpageId=<?= $wpageId ?>&zoomlevel=<?= $zoomlevel ?>"><?=$i ?></a>
                </li>

                <?php
                  }
                  //End Pagination Number

                  //Start Last
                  if ($page < $totalPages)
                  {
                ?>
                <li class="page-item">
                  <a class="page-link" href="<?=UrlwithoutPara()?>?pageNo=<?= $page+1 ?>&<?=$mediapara ?>=<?= $mediaarray ?>&productname=<?=$_name ?>&city=<?= $_city ?>&code=<?= $code ?>&wpageId=<?= $wpageId ?>&zoomlevel=<?= $zoomlevel ?>">></a>
                </li>
                <?php
                  }
                  if ($page < $totalPages){
                ?>

                <li class="page-item">
                  <a class="page-link" href="<?=UrlwithoutPara()?>?pageNo=<?= $totalPages ?>&<?=$mediapara ?>=<?= $mediaarray ?>&productname=<?=$_name ?>&city=<?= $_city ?>&code=<?= $code ?>&wpageId=<?= $wpageId ?>&zoomlevel=<?= $zoomlevel ?>">Last</a>
                </li>
                <?php
                  }
                  //End Last
                ?>

              </ul>
            </nav>
            <?php
              }
            ?>
            </div>
            <a href="<?= home_url('./contact') ?>?category=<?= the_field('page_info_title', $wpageId) ?>" class="submit-btn btn upper btn-search" >ENQUIRY</a>
          </div>

        </div>
      </section>

      <!-- Map -->
      <section class="service-map">
        <div class="container"> 
          <div class="map-width" id="map"></div>
        </div>
      </section>
      
<?php
    get_footer();
?>

<script type="text/javascript">
$(document).ready(function(){
    $('#mediaCode').multiSelect({
        noneText: 'Media Code'
    });
});
</script>
<!-- <script defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC5i0mt34lAKRc2J59PlC-M9T6TPyk817g&amp;callback=InitMap"></script> -->
<script defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyhLId3Kwr3MaJKoABzzuePVbcltp6Q-4&amp;callback=InitMap"></script>
<script>
  InitMap = () => {
    let map = new google.maps.Map(document.getElementById('map'), {
      center: {lat:<?=$Latitude ?>, lng: <?=$Longitude ?>},
      zoom: <?= $zoomlevel ?>
    })
    // let cursors = [
    //   {
    //     "Longitude":"96.134202",
    //     "Latitude":"16.900806",
    //     "MediaCode":"YIA-EXT-0027 ",
    //     "Location":"Terminal-3, outdoor, gate 27 air side area",
    //     "Link" : "kjdsfklfdjk"
    //   },
    //   {
    //     "Longitude":"96.131847",
    //     "Latitude":"16.898919",
    //     "MediaCode":"YIA-EXT-0028",
    //     "Location":"Terminal-3, outdoor, gate 29 air side area",
    //     "Link" : "kjdsfklfdjk"
    //   }
    // ]
  
    let cursors = <?= $jsondata ?>;
    cursors.map(cursor => {
      let marker = new google.maps.Marker({
        position: {
          lat: Number(cursor.Latitude),
          lng: Number(cursor.Longitude)
        }, 
        map,
        title: cursor.MediaCode
      })
      var infowindow = new google.maps.InfoWindow({
        content: `
          <h5>${cursor.MediaCode}</h5>
          <p>
            ${cursor.Location}<br/>
            <br/>
            <a href=${cursor.Link} >Go Detail <i class="fas fa-external-link-alt"></i></a>
          </p>
        `
      });
      marker.addListener('click', function() {
        infowindow.open(map, marker);
      })
    })
  }
</script>