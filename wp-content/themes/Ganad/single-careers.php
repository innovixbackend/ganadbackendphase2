<?php 

include_once "sendemail.php";
get_template_part('otherheader');
if(have_posts()):
  while(have_posts()):
    the_post(); 
?>
      <section class="career-detail career-page"> 
        <div class="container">
          <h5 class="title"><?= get_field("job_title") ?></h5>
          <div class="date-type"> 
            <ul class="type"> 
              <li> <strong class="type-label bold"><?php echo __("date", "ganad")  ?> </strong><span><?= get_field("job_date") ?></span></li>
              <li> <strong class="type-label bold"><?php echo __("department", "ganad")  ?> </strong><span><?= get_field("department") ?></span></li>
              <li> <strong class="type-label bold"><?php echo __("type", "ganad")  ?> </strong><span><?= get_field("type") ?></span></li>
              <li> <strong class="type-label bold"><?php echo __("location", "ganad")  ?> </strong><span><?= get_field("location") ?></span></li>
            </ul>
          </div>
          <div class="job-description"> 
            <h5 class="title"><?php echo __("responsibilities", "ganad")  ?> </h5>
            <p>
              <?= get_field("responsibilities") ?>
            </p>
            
          </div>
          <div class="job-description"> 
            <h5 class="title"><?php echo __("requirements", "ganad")  ?></h5>
            <p>
              <?= get_field("requirements") ?>
            </p>
            
          </div>



          <!-- application form -->
          <div class="job-form">
            <h5 class="title"><?php echo __("job_application", "ganad")  ?></h5>
            <div class="detail-form">
            <?php
              if($_POST['submit']) {
                $recaptcha =false;
                $success = false;
                if(isset($_POST['g-recaptcha-response'])){
                    $captcha=$_POST['g-recaptcha-response'];
                  }
                if(!$captcha){
                  $recaptcha=true;
                }

                $secretKey = "6LcNtroZAAAAAJDhqzDKfuZEx88XyNxvkbtx_xoO";
                // post request to server
                $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secretKey) .  '&response=' . urlencode($captcha);
                $response = file_get_contents($url);
                $responseKeys = json_decode($response,true);
                // should return JSON with success as true
                if($responseKeys["success"])
                {
                  $jobtitle = get_field("job_title");
                  $name = Validation($_POST['username']);
                  $phone = Validation($_POST['phone']);
                  $email = Validation($_POST['email']);
                  $applyposition = Validation($_POST['applyposition']);
                  $temp_name = $_FILES['attachfile']['tmp_name'];
                  $target_dir = get_template_directory().projectname()."/CVForm/";
                  $file = $_FILES['attachfile']['name'];
                  $path = pathinfo($file);
                  $filename = $path['filename'];
                  $ext = $path['extension'];
                  $path_filename_ext = $target_dir.$filename.".".$ext;

                  move_uploaded_file($temp_name,$path_filename_ext);
            
                  $success = applyform($jobtitle,$name, $phone, $email, $applyposition,$path_filename_ext);
                  
                }  
              }
              if($recaptcha){
                ?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>Error. </strong> Please fill google reCaptcha.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <?php
              }
                if($success)
                {
              ?>

              <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong> Your email have been sent. Thanks for contacting us!</strong>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
              </div>
            <?php }?>
              <form action="" method="POST" enctype = "multipart/form-data">
                <div class="form-group">
                  <label class="label" for="inputdefault"><?php echo __("name", "ganad")  ?>  </label><span class="star">*</span>
                  <input class="form-control" type="text" name="username" required>
                </div>
                <div class="form-group">
                  <label for="inputdefault1"><?php echo __("phone", "ganad")  ?></label><span class="star">*</span>
                  <input class="form-control"  type="text"  name="phone" required>
                </div>
                <div class="form-group">
                  <label for="inputdefault2"><?php echo __("email", "ganad")  ?></label><span class="star">*</span>
                  <input class="form-control" type="email" name="email" required>
                </div>
                <div class="form-group">
                  <label class="label" for="inputdefault"><?php echo __("apply_position", "ganad")  ?>  </label><span class="star">*</span>
                  <input class="form-control" type="text"  name="applyposition" required>
                </div>
                <div class="form-group"> 
                  <label class="label"><?php echo __("attached_file", "ganad")  ?> </label><span class="star">*</span>
                  <div class="attachment">
                    <div class="upload-file"><img src="<?php bloginfo('template_url'); ?>/assets/images/icons/attach.svg" alt=""></div>
                    <input id="myfile" type="file" name="attachfile" required>
                    <div class="showfile"><span class="filedata"></span></div>
                  </div>
                </div>
                <div class="form-group g-recaptcha" data-sitekey="6LcNtroZAAAAAGCKeKokk7ZJYkieDoCyoulqkU3k"></div>
                <div class="sharelink-wrap"> 
                  <div class="sharelink" id="shareBlock"></div>
                  <div class="form-button careerBtn">
                    <button class="submit-btn btn upper"  type="submit" name="submit" value="submit"><?php echo __("apply", "ganad")  ?></button>
                    <button class="submit-btn btn upper shareBtn" type="button"><?php echo __("share", "ganad")  ?> </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
<?php
    endwhile;
    endif;
    get_footer();
?>