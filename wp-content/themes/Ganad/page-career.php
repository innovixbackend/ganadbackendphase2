<?php 
get_template_part('otherheader');
if(have_posts()):
  while(have_posts()):
    the_post(); 
    $page_title = get_field("page_title");
?>
      <section class="career">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <h3 class="sub-header upper pageheader"><?= $page_title ?></h3>
            </div>
            <div class="col-12">
              <div class="career-content">
                <p><?= nl2br(get_field("page_description")) ?></p>
                
              </div>
            </div>
            <?php get_template_part('Partial/_position');?>
            
          </div>
        </div>
      </section>


<?php
    endwhile;
    endif;
    get_footer();
?>