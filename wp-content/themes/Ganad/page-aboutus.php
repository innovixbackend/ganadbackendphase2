<?php 
    get_template_part('otherheader');
    if(have_posts()):
    while(have_posts()):
        the_post(); 
        $page_title = get_field("page_title");
        endwhile;
    endif;
?>
    <section class="aboutus-header">
        <div class="container">
            <h3 class="sub-header upper pageheader"><?= $page_title ?></h3>
        </div>
    </section>
    <?php 
        get_template_part('Partial/_branchesmap');
        get_template_part('Partial/_ourstory');
        get_template_part('Partial/_ourhistory');
        get_template_part('Partial/_ourclient');  
    get_footer();
?>
