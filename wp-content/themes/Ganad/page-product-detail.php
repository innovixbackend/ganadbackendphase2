<?php
    get_template_part('otherheader');
    $sId ="8";
    if (isset($_GET['productId'])){
      $sId =Validation($_GET['productId']);
    }

    $__pDetail ="SELECT * FROM sitelist WHERE SId='$sId'";
    $__pDetailresult = $wpdb->get_results($__pDetail);
    if(!empty($__pDetailresult)){
      foreach($__pDetailresult as $print) {
        $MediaCode = $print->MediaCode;
        $MediaType = $print->MediaType;
        $Longitude = $print->Longitude;
        $Latitude = $print->Latitude;
        $TrafficMovement = $print->TrafficMovement;
        $ActualWidth = $print->ActualWidth;
        $ActualHeight = $print->ActualHeight;
        $City = $print->City;
        $StateAndDivision = $print->StateAndDivision;
        $Location = $print->Location;
        $YoutubeLink = $print->YoutubeLink;
        $MapUrl = $print->MapUrl;
        // if(!empty($print->MapUrl))
        // {
        //   $MapUrl = $print->MapUrl;
        // }
      }
      $defaultimg = "/SiteList/DefaultImg/default2.jpg";
      $__img ="SELECT * FROM detailimages WHERE SId='$sId'";
      $__imgresult = $wpdb->get_results($__img);
?>
	<style>
		.home-firsection .firstsec-slider .slick-prev {
			background-image: url(<?php bloginfo('template_url'); ?>/assets/images/icons/next1.svg);
		}
		.home-firsection .firstsec-slider .slick-next {
			background-image: url(<?php bloginfo('template_url'); ?>/assets/images/icons/next1.svg);
		}
	</style>
  <?php
    if(!empty($YoutubeLink)){
  ?>
  <section class="home-firsection section-0 contactus-banner banner-video" id="homeSection-1">
  <div class="firstsec-slider">
    <div class="first-sec mt-0">
      <div class="firstsec-img">
        <div class="overlay"></div>
        <iframe width="100%" frameborder="0px" height="705px" src="<?= $YoutubeLink ?>" allowfullscreen></iframe>
        <div class="slide-desc-wrap">
          <div class="slide-desc">
            <h2><?= $MediaCode ?></h2>
          </div>
        </div>
      </div>
    </div>
    </div>
</section>
    <?php
          }
    else{
      ?>
      <section class="home-firsection section-0 contactus-banner" id="homeSection-1">
  <div class="firstsec-slider">
  <?php
        if(!empty($__imgresult))
        {
          foreach ($__imgresult as $img)
          {
          ?>
          <div class="first-sec mt-0">
            <div class="firstsec-img">
              <!-- <div class="overlay"></div> -->
              <img class="img-fluid img-desk" src="<?php bloginfo('template_url'); ?><?= $img->ImagePath ?>" alt="">
              <img class="img-fluid img-mb" src="<?php bloginfo('template_url'); ?><?= $img->ImagePath ?>" alt="">
              <div class="slide-desc-wrap">
                <div class="slide-desc">
                  <h2><?= $MediaCode ?></h2>
                </div>
              </div>
            </div>
          </div>
         <?php 
          }
        }
        else{

        ?>
        <div class="first-sec mt-0">
          <div class="firstsec-img">
            <!-- <div class="overlay"></div> -->
            <img class="img-fluid img-desk" src="<?php bloginfo('template_url'); ?><?= $defaultimg ?>" alt="">
            <img class="img-fluid img-mb" src="<?php bloginfo('template_url'); ?><?= $defaultimg ?>" alt="">
            <div class="slide-desc-wrap">
              <div class="slide-desc">
                <h2><?= $MediaCode ?></h2>
              </div>
            </div>
          </div>
        </div>
      <?php
        }
        ?>
         </div>
</section>
<?php
  }
?>
           



<section class="ourwork_detail"> 
  <div class="container"> 
    <div class="row"> 
      <div class="col-lg-7"> 
        <div class="title-border">
          <h5 class="bold">Location Map</h5>
        </div>
        <div class="work_map">
          <section class="service-map">
            <div class="container"> 
              <div class="map-width" id="map"></div>
            </div>
          </section>
          <?php
            if(!empty($MapUrl))
            {
          ?>
          <a href="<?=  $MapUrl ?>" target="_blank">
            <p><?=  $Location ?></p>
          </a>
          <?php
            }
            else{
          ?>
           <a>
            <p><?=  $Location ?></p>
          </a>
          <?php
          }
          ?>
        </div>
      </div>
      <div class="col-lg-5">
        <div class="title-border detail_informatbion_order">
          <h5 class="bold">Details</h5>
        </div>
        <div class="detail_information">
          <ul> 
            <li> <strong>Media Type :</strong><span><?=  $MediaType ?></span></li>
            <li> <strong>Media Code :</strong><span><?=  $MediaCode ?></span></li>
            <li> <strong>Traffic Moment :</strong><span><?=  $TrafficMovement ?></span></li>
            <li> <strong>Actual Width :</strong><span><?=  $ActualWidth ?> ft</span></li>
            <li> <strong>Actual Height :</strong><span><?=  $ActualHeight ?> ft</span></li>
            <li> <strong>City :</strong><span><?=  $City ?></span></li>
            <li> <strong>Division/State :</strong><span><?=  $StateAndDivision ?></span></li>
            <div class="detail-button">
              <div class="sharelink-wrap"> </div>
              <div class="sharelink" id="shareBlock"></div>
              <div class="form-button careerBtn">
                <button class="submit-btn btn upper shareBtn" type="button">Share</button>
                <a href="<?= home_url('./contact') ?>?mediaCode=<?=$MediaCode ?>" class="submit-btn btn upper">ENQUIRY</a>
              </div>
            </div>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<?php
    }
     get_footer();
?>
<!-- <script defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC5i0mt34lAKRc2J59PlC-M9T6TPyk817g&amp;callback=InitMap"></script> -->
<script defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyhLId3Kwr3MaJKoABzzuePVbcltp6Q-4&amp;callback=InitMap"></script>
<script>
  InitMap = () => {
    let map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: <?=  $Latitude ?>, lng: <?=  $Longitude ?>},
      zoom: 17
    })
    let cursors = [
      {
        "Longitude":"<?=  $Longitude ?>",
        "Latitude":"<?=  $Latitude ?>",
        "MediaCode":"<?=  $MediaCode ?>",
        "Location":"<?=  $City ?>"
      }
    ]
  
    cursors.map(cursor => {
      let marker = new google.maps.Marker({
        position: {
          lat: Number(cursor.Latitude),
          lng: Number(cursor.Longitude)
        }, 
        map,
        title: cursor.MediaCode
      })
      var infowindow = new google.maps.InfoWindow({
        content: `
          <h5>${cursor.MediaCode}</h5>
          <p>
            ${cursor.Location}
          </p>
        `
      });
      marker.addListener('click', function() {
        infowindow.open(map, marker);
      })
    })
  }
</script>